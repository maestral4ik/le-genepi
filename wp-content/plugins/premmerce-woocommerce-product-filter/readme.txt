=== Premmerce WooCommerce Product Filter ===

Contributors: premmerce
Tags: product filter, WooCommerce product filter, WooCommerce filter,  Premmerce, attributes filter
Requires at least: 4.8
Tested up to: 4.9.2
Stable tag: 1.0.4
Requires PHP: 5.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Premmerce WooCommerce Product Filter plugin is a convenient and flexible tool for managing filters for WooCommerce products. Among the main features of this plugin there is a single widget that manages the display of all available filters. Comparing to the standard WooCommerce filters, Premmerce WooCommerce Product Filter has a well thought out caching system for the load speed improving.

== Description ==
Premmerce WooCommerce Product Filter plugin is a convenient and flexible tool for managing filters for WooCommerce products. Among the main features of this plugin there is a single widget that manages the display of all available filters. Comparing to the standard WooCommerce filters, Premmerce WooCommerce Product Filter has a well thought out caching system for the load speed improving.

Full documentation is available here: [Premmerce WooCommerce Product Filter](https://premmerce.com/woocommerce-product-filter/)

We recommend that you  look through the following research:
[The comparison of the load speed of the WooCommerce filters and Premmerce WooCommerce Product Filter with the cache plugins](https://premmerce.com/top-3-woocommerce-cache-w3-total-cache-wp-super-cache-wp-rocket-comparison/)

= Major features of “Premmerce WooCommerce Product Filter” =

* displaying all available filters using a single widget
* integration with the Premmerce WooCommerce Brands plugin
* improved caching system
* displaying only available attributes depending on the category
* an option to filter the search results

= Demo =

You can see how it works on the Storefront theme here: <https://storefront.premmerce.com/electronics/mobile/>   .

You can see how it works on the SalesZone theme here: <https://saleszone.premmerce.com/electronics/mobile/>   .

Plus, you can create your personal demo store and test  this plugin together with [Premmerce Premium](https://premmerce.com/features/) and all other Premmerce plugins and themes  developed by our team here:  [Premmerce WooCommerce Demo](https://premmerce.com/premmerce-woocommerce-demo/).

= Compatibility with the other Plugins =

* WooCommerce
* Premmerce WooCommerce Brands
* Premmerce Search

= Installation =

1. Unzip the downloaded zip file.
1. Upload the plugin folder into the ‘wp-content/plugins/’ directory of your WordPress site.
1. Activate ‘Premmerce WooCommerce Product Filter’ from the Plugins page

== Screenshots ==

1. The filter widget settings;
2. The filter displaying on the product category page;
3. The plugin settings in the administrative area.

== Changelog ==

= 1.0 =

Release Date: Oct 19, 2017

* Initial release

= 1.0.1 =

Release Date: Nov 20, 2017

* Added WordPress 4.9 support
* Added WooCommerce 3.2.4 support

= 1.0.2 =

Release Date: Dec 06, 2017

* Added Premmerce icon to menu
* Added Premmerce version check
* Added WordPress 4.9.1 support
* Added WooCommerce 3.2.5 support

= 1.0.3 =

Release Date: Jan 24, 2018

* Updated freemius sdk
* Fixed the dependency check on multisite
* Fixed "Clear cache" button

= 1.0.4 =

Release Date: Feb 02, 2018

* Added WooCommerce 3.3.0 support
* Updated translations

== Translators ==

= Available Languages =

* English (Default)
* Russian
* Ukrainian

== Documentation ==

Full documentation is available here: [Premmerce WooCommerce Product Filter](https://premmerce.com/woocommerce-product-filter/)
