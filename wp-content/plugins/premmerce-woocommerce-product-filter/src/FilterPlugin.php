<?php namespace Premmerce\Filter;

use Premmerce\Filter\Admin\Admin;
use Premmerce\Filter\Filter\FilterCache;
use Premmerce\Filter\Filter\IntersectFilter;
use Premmerce\Filter\Widget\FilterWidget;
use Premmerce\Filter\WordpressSDK\FileManager\FileManager;
use Premmerce\Filter\WordpressSDK\Notifications\AdminNotifier;
use Premmerce\Filter\WordpressSDK\Plugin\PluginInterface;

/**
 * Class FilterPlugin
 *
 * @package Premmerce\Filter
 */
class FilterPlugin implements PluginInterface{

	const DOMAIN = 'premmerce-filter';

	const OPTION_ATTRIBUTES = 'premmerce_filter_attributes';
	const OPTION_BRANDS = 'premmerce_filter_brands';
	const OPTION_SETTINGS = 'premmerce_filter_settings';

	const BRAND_TAXONOMY = 'product_brand';

	/**
	 * @var FileManager
	 */
	private $fileManager;

	/**
	 * @var AdminNotifier
	 */
	private $notifier;

	/**
	 * PluginManager constructor.
	 *
	 * @param string $mainFile
	 */
	public function __construct($mainFile){
		$this->fileManager = new FileManager($mainFile);
		$this->notifier    = new AdminNotifier();

		add_action('init', [$this, 'loadTextDomain']);
		add_action('admin_init', [$this, 'checkRequirePlugins']);
	}

	/**
	 * Run plugin part
	 */
	public function run(){

		$valid = count($this->validateRequiredPlugins()) == 0;

		if($valid){
			$this->registerHooks();
		}
		if(is_admin()){
			new Admin($this->fileManager);
		}
	}

	public function registerHooks(){
		$intersectFilter = new IntersectFilter();

		add_action('widgets_init', function() use ($intersectFilter){
			register_widget(new FilterWidget($this->fileManager, $intersectFilter));
		});


		add_filter('posts_search', [$intersectFilter, 'onPostsSearch'], 100);

		add_filter('woocommerce_product_query_tax_query', [$intersectFilter, 'onWooCommerceTaxQuery']);
	}

	/**
	 * Fired when the plugin is activated
	 */
	public function activate(){

		if(!get_option(self::OPTION_SETTINGS)){
			$defaultOptions = [
				"hide_empty"    => "1",
				"product_cat"   => "1",
				"tag"           => "1",
				"product_brand" => "1",
				"search"        => "1",
				"shop"          => "1",
			];
			add_option(self::OPTION_SETTINGS, $defaultOptions);
		}
	}

	/**
	 * Fired when the plugin is deactivated
	 */
	public function deactivate(){
		$cache = new FilterCache();
		$cache->clear();
		rmdir($cache->getCacheDir());
	}

	/**
	 * Fired during plugin uninstall
	 */
	public static function uninstall(){
		delete_option(self::OPTION_BRANDS);
		delete_option(self::OPTION_ATTRIBUTES);
		delete_option(self::OPTION_SETTINGS);
	}

	/**
	 * Check required plugins and push notifications
	 */
	public function checkRequirePlugins(){
		$message = __('The %s plugin requires %s plugin to be active!', 'premmerce-filter');

		$plugins = $this->validateRequiredPlugins();

		if(count($plugins)){
			foreach($plugins as $plugin){
				$error = sprintf($message, 'Premmerce WooCommerce Product Filter', $plugin);
				$this->notifier->push($error, AdminNotifier::ERROR, false);
			}
		}

	}

	/**
	 * Validate required plugins
	 *
	 * @return array
	 */
	private function validateRequiredPlugins(){

		$plugins = [];

		if(!function_exists('is_plugin_active')){
			include_once(ABSPATH . 'wp-admin/includes/plugin.php');
		}

		/**
		 * Check if WooCommerce is active
		 **/
		if(!(is_plugin_active('woocommerce/woocommerce.php') || is_plugin_active_for_network('woocommerce/woocommerce.php'))){
			$plugins[] = '<a target="_blank" href="https://wordpress.org/plugins/woocommerce/">WooCommerce</a>';
		}

		return $plugins;
	}

	/**
	 * Load plugin translations
	 */
	public function loadTextDomain(){
		$name = $this->fileManager->getPluginName();
		load_plugin_textdomain('premmerce-filter', false, $name . '/languages/');
	}
}
