<?php namespace Premmerce\Filter\Admin;

use Premmerce\Filter\FilterPlugin;
use Premmerce\Filter\Filter\IntersectFilter;
use Premmerce\Filter\WordpressSDK\FileManager\FileManager;

/**
 * Class Admin
 *
 * @package Premmerce\Filter\Admin
 */
class Admin{

	/**
	 * @var FileManager
	 */
	private $fileManager;

	/**
	 * @var string
	 */
	private $settingsPage;

	/**
	 * @var IntersectFilter
	 */
	private $filter;

	/**
	 * @var array
	 */
	private $defaultAttribute = ['active' => false, 'type' => 'checkbox'];

	/**
	 * @var array
	 */
	private $defaultBrand = ['active' => false];

	private $bulkActions = [
		'display'  => ['active' => 1],
		'hide'     => ['active' => 0],
		'checkbox' => ['type' => 'checkbox'],
		'select'   => ['type' => 'select'],
		'radio'    => ['type' => 'radio'],
	];

	/**
	 * Admin constructor.
	 *
	 * Register menu items and handlers
	 *
	 * @param FileManager $fileManager
	 */
	public function __construct(FileManager $fileManager){
		$this->fileManager = $fileManager;

		$this->filter = new IntersectFilter();

		$this->settingsPage = FilterPlugin::DOMAIN . '-admin';

		add_action('admin_menu', [$this, 'addMenuPage']);
		add_action('admin_menu', [$this, 'addFullPack'], 100);

		add_action('wp_ajax_premmerce_filter_bulk_action_attributes', [$this, 'bulkActionAttributes']);
		add_action('wp_ajax_premmerce_filter_bulk_action_brands', [$this, 'bulkActionBrands']);

		add_action('wp_ajax_premmerce_filter_sort_attributes', [$this, 'sortAttributes']);
		add_action('wp_ajax_premmerce_filter_sort_brands', [$this, 'sortBrands']);

		add_action('admin_post_premmerce_filter_save_settings', [$this, 'saveSettings']);
		add_action('admin_post_premmerce_filter_cache_warmup', [$this, 'warmUpCache']);

		add_action('woocommerce_update_product', [$this, 'clearCache']);
		add_action('woocommerce_update_product_variation', [$this, 'clearCache']);
	}

	/**
	 * Ajax update attributes ordering
	 */
	public function sortAttributes(){
		$this->sortHandler(FilterPlugin::OPTION_ATTRIBUTES, $this->getAttributesConfig());
	}

	/**
	 * Ajax update brands ordering
	 */
	public function sortBrands(){
		$this->sortHandler(FilterPlugin::OPTION_BRANDS, $this->getBrandsConfig());
	}

	/**
	 * Ajax bulk update brands
	 */
	public function bulkActionBrands(){
		$this->bulkActionsHandler(FilterPlugin::OPTION_BRANDS, $this->getBrandsConfig());
	}

	/**
	 * Ajax bulk update attributes
	 */
	public function bulkActionAttributes(){
		$this->bulkActionsHandler(FilterPlugin::OPTION_ATTRIBUTES, $this->getAttributesConfig());
	}


	/**
	 * Options page
	 */
	public function options(){
		$woocommerceActive = in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')));

		$brandsActive = taxonomy_exists(FilterPlugin::BRAND_TAXONOMY);

		$defaultTab = $woocommerceActive? 'attributes' : 'settings';

		$current = isset($_GET['tab'])? $_GET['tab'] : $defaultTab;

		$data = [];
		if($woocommerceActive){
			$tabs['attributes'] = __('Attributes', FilterPlugin::DOMAIN);
			if('attributes' === $current){
				$data = $this->attributeTabData();
			}
		}

		if($brandsActive){
			$tabs['brands'] = __('Brands', FilterPlugin::DOMAIN);
			if('brands' === $current){
				$data = $this->brandTabData();
			}
		}

		$tabs['settings'] = __('Settings', FilterPlugin::DOMAIN);

		if(function_exists('premmerce_pwpf_fs')){
			$tabs['contact'] = __('Contact Us', 'premmerce-filter');
			if(premmerce_pwpf_fs()->is_registered()){
				$tabs['account'] = __('Account', 'premmerce-filter');
			}
		}

		if('settings' === $current){
			$data = $this->settingsTabData();
		}

		$data['tabs']    = $tabs;
		$data['current'] = $current;

		$this->registerAssets();

		$this->fileManager->includeTemplate('admin/options.php', $data);
	}


	/**
	 * Renders attribute tab template
	 *
	 * @return string
	 */
	public function attributeTabData(){
		$attributesConfig = $this->getAttributesConfig();
		$attributes       = array_replace($attributesConfig, $this->getAttributes());

		$types = [
			'checkbox' => 'Checkbox',
			'radio'    => 'Radio',
			'select'   => 'Dropdown',
		];

		$actions = [
			"-1"       => __('Bulk Actions', FilterPlugin::DOMAIN),
			"display"  => __('Display', FilterPlugin::DOMAIN),
			"hide"     => __('Hide', FilterPlugin::DOMAIN),
			"checkbox" => 'Checkbox',
			"select"   => 'Dropdown',
			"radio"    => 'Radio',
		];

		$dataAction = 'premmerce_filter_bulk_action_attributes';

		return compact('attributes', 'attributesConfig', 'types', 'actions', 'dataAction');
	}

	/**
	 * Renders brand tab template
	 *
	 * @return string
	 */
	public function brandTabData(){
		$brandsConfig = $this->getBrandsConfig();
		$brands       = array_replace($brandsConfig, $this->getBrands());

		$actions = [
			"-1"      => __('Bulk Actions'),
			"display" => __('Display', FilterPlugin::DOMAIN),
			"hide"    => __('Hide', FilterPlugin::DOMAIN),
		];

		$dataAction = 'premmerce_filter_bulk_action_brands';

		return compact('brands', 'brandsConfig', 'actions', 'dataAction');
	}

	/**
	 * Renders settings tab template
	 *
	 * @return string
	 */
	public function settingsTabData(){
		$settings = get_option(FilterPlugin::OPTION_SETTINGS, []);

		return compact('settings');
	}


	/**
	 * Settings tab handler
	 */
	public function saveSettings(){
		$data = $_POST;

		$key = FilterPlugin::OPTION_SETTINGS;

		$attributes = isset($data[ $key ])? $data[ $key ] : [];

		update_option($key, $attributes);

		wp_redirect($_SERVER['HTTP_REFERER']);
	}


	public function clearCache(){
		$this->filter->clearCache();
	}

	/**
	 * Warm up button handler
	 */
	public function warmUpCache(){
		$this->filter->clearCache();
		wp_redirect($_SERVER['HTTP_REFERER']);
	}

	/**
	 * Add submenu to premmerce menu page
	 */
	public function addMenuPage(){
		global $admin_page_hooks;

		$premmerceMenuExists = isset($admin_page_hooks['premmerce']);


		if(!$premmerceMenuExists){

			$svg = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="20" height="16" style="fill:#82878c" viewBox="0 0 20 16"><g id="Rectangle_7"> <path d="M17.8,4l-0.5,1C15.8,7.3,14.4,8,14,8c0,0,0,0,0,0H8h0V4.3C8,4.1,8.1,4,8.3,4H17.8 M4,0H1C0.4,0,0,0.4,0,1c0,0.6,0.4,1,1,1 h1.7C2.9,2,3,2.1,3,2.3V12c0,0.6,0.4,1,1,1c0.6,0,1-0.4,1-1V1C5,0.4,4.6,0,4,0L4,0z M18,2H7.3C6.6,2,6,2.6,6,3.3V12 c0,0.6,0.4,1,1,1c0.6,0,1-0.4,1-1v-1.7C8,10.1,8.1,10,8.3,10H14c1.1,0,3.2-1.1,5-4l0.7-1.4C20,4,20,3.2,19.5,2.6 C19.1,2.2,18.6,2,18,2L18,2z M14,11h-4c-0.6,0-1,0.4-1,1c0,0.6,0.4,1,1,1h4c0.6,0,1-0.4,1-1C15,11.4,14.6,11,14,11L14,11z M14,14 c-0.6,0-1,0.4-1,1c0,0.6,0.4,1,1,1c0.6,0,1-0.4,1-1C15,14.4,14.6,14,14,14L14,14z M4,14c-0.6,0-1,0.4-1,1c0,0.6,0.4,1,1,1 c0.6,0,1-0.4,1-1C5,14.4,4.6,14,4,14L4,14z"/></g></svg>';
			$svg = 'data:image/svg+xml;base64,' . base64_encode($svg);

			add_menu_page(
				'Premmerce',
				'Premmerce',
				'manage_options',
				'premmerce',
				'',
				$svg
			);
		}

		add_submenu_page(
			'premmerce',
			__('Product filter', 'premmerce-filter'),
			__('Product filter', 'premmerce-filter'),
			'manage_options',
			$this->settingsPage,
			[$this, 'options']
		);

		if(!$premmerceMenuExists){
			global $submenu;
			unset($submenu['premmerce'][0]);
		}
	}

	public function addFullPack(){
		global $submenu;

		if(!function_exists('get_plugins')){
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

		$plugins = get_plugins();

		$premmerceInstalled = array_key_exists('premmerce-premium/premmerce.php', $plugins)
		                      || array_key_exists('premmerce/premmerce.php', $plugins);

		if(!$premmerceInstalled){
			$submenu['premmerce'][999] = [
				'Get premmerce full pack',
				'manage_options',
				admin_url('plugin-install.php?tab=plugin-information&plugin=premmerce'),
			];
		}
	}

	/**
	 * Ajax order by ids handler
	 *
	 * @param string $key - options key to update
	 * @param array $actual - actual data
	 *
	 */
	private function sortHandler($key, $actual){
		$ids = isset($_POST['ids'])? $_POST['ids'] : null;

		if(is_array($ids)){
			$ids = array_combine($ids, $ids);

			$config = array_replace($ids, $actual);

			update_option($key, $config);
		}

		wp_die();
	}

	/**
	 * Bulk update entities
	 *
	 * @param string $key - config key
	 * @param array $config - initial config
	 */
	private function bulkActionsHandler($key, $config){
		$action = isset($_POST['value'])? $_POST['value'] : null;
		$ids    = isset($_POST['ids'])? $_POST['ids'] : [];

		if(array_key_exists($action, $this->bulkActions)){
			$update = $this->bulkActions[ $action ];
			foreach($ids as $id){
				if(array_key_exists($id, $config)){
					$config[ $id ] = array_merge($config[ $id ], $update);
				}
			}
			update_option($key, $config);
		}

		wp_die();
	}

	/**
	 * Get attributes configuration
	 *
	 * @return mixed
	 */
	private function getAttributesConfig(){
		return $this->getConfig(FilterPlugin::OPTION_ATTRIBUTES, $this->getAttributes(), $this->defaultAttribute);
	}


	/**
	 * Get brands configuration
	 *
	 * @return mixed
	 */
	private function getBrandsConfig(){
		return $this->getConfig(FilterPlugin::OPTION_BRANDS, $this->getBrands(), $this->defaultBrand);
	}

	/**
	 * Get config with actual values
	 *
	 * @param $name
	 * @param $actual
	 * @param $default
	 *
	 * @return array
	 */
	private function getConfig($name, $actual, $default){
		$config = get_option($name, []);

		if(!is_array($config)){
			$config = [];
		}

		$ids       = array_keys($actual);
		$configIds = array_keys($config);

		$removed = array_diff($configIds, $ids);

		foreach($removed as $id){
			unset($config[ $id ]);
		}

		$new = array_diff($ids, $configIds);

		foreach($new as $id){
			$config[ $id ] = $default;
		}

		return $config;
	}

	/**
	 * Premmerce Brands id=>title array
	 *
	 * @return array
	 */
	private function getBrands(){
		$brands = get_terms([
			'taxonomy'   => FilterPlugin::BRAND_TAXONOMY,
			'fields'     => 'id=>name',
			'orderby'    => 'name',
			'order'      => 'ASC',
			'hide_empty' => false,
		]);

		return is_array($brands)? $brands : [];
	}

	/**
	 * Woocommerce attributes id=>title array and brand taxonomy if exists
	 *
	 * @return array
	 */
	private function getAttributes(){
		$wcAttributes = wc_get_attribute_taxonomies();

		$attributes = [];
		foreach($wcAttributes as $attribute){
			$attributes[ $attribute->attribute_id ] = $attribute->attribute_label;
		}

		if(taxonomy_exists(FilterPlugin::BRAND_TAXONOMY)){
			$brandTaxonomy                      = get_taxonomy(FilterPlugin::BRAND_TAXONOMY);
			$attributes[ $brandTaxonomy->name ] = $brandTaxonomy->label;
		}

		return $attributes;
	}

	/**
	 * Register admin css and js
	 */
	private function registerAssets(){
		wp_enqueue_script('premmerce_filter_admin_script', $this->fileManager->locateAsset('admin/js/script.js'), ['jquery-ui-sortable']);
		wp_enqueue_style('premmerce_filter_admin_style', $this->fileManager->locateAsset('admin/css/style.css'));
	}
}
