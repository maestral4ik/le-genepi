<?php namespace Premmerce\Filter\Widget;

use Premmerce\Filter\FilterPlugin;
use Premmerce\Filter\Filter\IntersectFilter;
use Premmerce\Filter\WordpressSDK\FileManager\FileManager;

class FilterWidget extends \WP_Widget{

	/**
	 * @var FileManager
	 */
	private $fileManager;
	/**
	 * @var IntersectFilter
	 */
	private $intersectFilter;

	/**
	 * FilterWidget constructor.
	 *
	 * @param FileManager $fileManager
	 * @param IntersectFilter $intersectFilter
	 */
	public function __construct(FileManager $fileManager, IntersectFilter $intersectFilter){
		parent::__construct(
			'premmerce_filter_filter_widget',
			__('Premmerce filter', 'premmerce-filter'),
			[
				'description' => __('Product attributes filter', 'premmerce-filter'),
			]
		);

		$this->fileManager     = $fileManager;
		$this->intersectFilter = $intersectFilter;
	}

	/**
	 * @param array $args
	 * @param array $instance
	 */
	public function widget($args, $instance){
		$settings = get_option(FilterPlugin::OPTION_SETTINGS);

		$category = isset($settings['product_cat']) && is_tax('product_cat');

		$search = isset($settings['search']) && is_search();

		$tag = isset($settings['tag']) && is_product_tag();

		$shop = isset($settings['shop']) && is_shop() && !is_search();

		$brand = isset($settings['product_brand']) && is_tax(FilterPlugin::BRAND_TAXONOMY);

		if($category || $search || $tag || $brand || $shop){
			$attributes = $this->intersectFilter->getItems($settings);

			$this->fileManager->includeTemplate('frontend/filter.php', [
				'args'       => $args,
				'attributes' => $attributes,
				'instance'   => $instance,
			]);
		}
	}

	/**
	 * @param array $new_instance
	 * @param array $old_instance
	 *
	 * @return array
	 */
	public function update($new_instance, $old_instance){
		$instance          = [];
		$instance['title'] = strip_tags($new_instance['title']);

		return $instance;
	}

	/**
	 * @param array $instance
	 *
	 * @return string|void
	 */
	public function form($instance){
		$this->fileManager->includeTemplate('admin/filter-widget.php', [
			'title'  => isset($instance['title'])? $instance['title'] : '',
			'widget' => $this,
		]);
	}
}
