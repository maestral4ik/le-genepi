<?php namespace Premmerce\Filter\Filter;

use Premmerce\Filter\FilterPlugin;
use stdClass;
use WC_Query;
use WP_Error;
use WP_Meta_Query;
use WP_Tax_Query;
use WP_Term;

class FilterModel
{

    /**
     * @var array
     */
    private $attributeSettings;

    /**
     * @var array
     */
    private $brandSettings;

    /**
     * @var FilterCache
     */
    private $cache;

    /**
     * FilterModel constructor.
     *
     * @param FilterCache $cache
     */
    public function __construct(FilterCache $cache)
    {
        $this->cache = $cache;

        $activeFilter = function ($item) {
            return $item['active'] == true;
        };

        $this->attributeSettings = array_filter(get_option(FilterPlugin::OPTION_ATTRIBUTES, []), $activeFilter);

        $this->brandSettings = array_filter(get_option(FilterPlugin::OPTION_BRANDS, []), $activeFilter);
    }


    /**
     * Clear filter cache
     */
    public function clearCache()
    {
        $this->cache->clear();
    }

    /**
     * Warm up filter cache
     */
    public function warmUpCache()
    {
        $this->cache->clear();

        $ids = get_terms(['taxonomy' => 'product_cat', 'fields' => 'ids']);

        $attributes     = $this->getSortedAttributes();
        $attributeNames = array_keys($attributes);

        if (is_array($ids) && is_array($attributes)) {
            foreach ($ids as $id) {
                $this->getTermProductIdsByTaxonomy($attributeNames, $id);
            }
        }
    }

    /**
     * Filter attributes by selected filter values
     *
     * @return array
     */
    public function getAttributes()
    {
        $attributes = $this->getSortedAttributes();

        $results = [];
        foreach ($attributes as $id => $attribute) {
            $attribute->html_type         = $this->getHtmlType($id);
            $attribute->filter_query_type = $this->getQueryType($attribute->html_type);
            $attribute->traxonomy_name    = $this->getTaxonomyName($attribute);
            $attribute->reset_url         = $this->getCurrentUrl($attribute->attribute_name);

            $results[ $attribute->traxonomy_name ] = $attribute;
        }


        return $results;
    }

    /**
     * Get terms for array of taxonomies
     *
     * @param array $taxonomies
     *
     * @return array
     */
    public function getTerms(array $taxonomies)
    {
        $terms = [];

        foreach ($taxonomies as $taxonomy) {
            $terms[ $taxonomy->traxonomy_name ] = $this->getTaxonomyTerms($taxonomy, $taxonomy->attribute_orderby);
        }

        return $terms;
    }

    /**
     * Get terms for taxonomy ordered by settings
     *
     * @param stdClass $taxonomy
     * @param string $orderBy
     *
     * @return array
     */
    public function getTaxonomyTerms(stdClass $taxonomy, $orderBy)
    {
        $taxonomyName = $taxonomy->traxonomy_name;

        if ($taxonomy->attribute_name === FilterPlugin::BRAND_TAXONOMY) {
            $taxonomyTerms = $this->getBrandTerms($taxonomyName, $orderBy);
        } else {
            $taxonomyTerms = $this->getAttributeTerms($taxonomyName, $orderBy);
        }

        $result = [];

        $termValues = $this->getTermValuesFromUrl($taxonomy);

        foreach ($taxonomyTerms as $key => $term) {
            $term->checked            = array_search($term->slug, $termValues) !== false;
            $term->link               = $this->getTermLink($term->slug, $term->checked, $taxonomy, $termValues);
            $result[ $term->term_id ] = $term;
        }

        return $result;
    }

    /**
     * Product ids selected by main WP query.
     *
     * @param array $taxonomies
     * @param string $searchQuery
     *
     * @return array
     */
    public function getProductIdsByMainQuery($taxonomies, $searchQuery)
    {
        global $wpdb;

        $tax_query_sql  = $this->getTaxQuerySql($taxonomies);
        $meta_query_sql = $this->getMetaQuerySql();

        $query   = [];
        $query[] = "SELECT DISTINCT {$wpdb->posts}.ID id FROM {$wpdb->posts}";

        $query[] = $tax_query_sql['join'];
        $query[] = $meta_query_sql['join'];

        $query[] = "WHERE {$wpdb->posts}.post_type IN ( 'product' )";
        $query[] = "AND {$wpdb->posts}.post_status = 'publish'";
        $query[] = $tax_query_sql['where'];
        $query[] = $meta_query_sql['where'];


        if ($search = $searchQuery) {
            $query[] = $search;
        }


        $query = implode(' ', $query);

        $cacheKey = md5($query);

        if ($results = $this->cache->get($cacheKey)) {
            return $results;
        }

        $results = $wpdb->get_col($query);

        $this->cache->set($cacheKey, $results);

        return $results;
    }

    /**
     * Term values in url get parameters
     *
     * @param stdClass $taxonomy
     *
     * @return array
     */
    private function getTermValuesFromUrl(stdClass $taxonomy)
    {
        $taxonomyNameClear = sanitize_title($taxonomy->attribute_name);

        $taxonomyFilter = 'filter_' . $taxonomyNameClear;

        $termValues = isset($_GET[ $taxonomyFilter ])? explode(',', wc_clean($_GET[ $taxonomyFilter ])) : [];

        $termValues = array_map('sanitize_title', $termValues);

        return $termValues;
    }


    /**
     * Link for term to select or deselect term
     *
     * @param string $termSlug
     * @param bool $termSelected
     * @param stdClass $taxonomy
     * @param array $termValues
     *
     * @return WP_Term
     */
    private function getTermLink($termSlug, $termSelected, stdClass $taxonomy, array $termValues)
    {
        $taxonomyNameClear = sanitize_title($taxonomy->attribute_name);

        $taxonomyFilter = 'filter_' . $taxonomyNameClear;

        $taxonomyQueryType = 'query_type_' . $taxonomyNameClear;


        $termKey = array_search($termSlug, $termValues);


        if ($taxonomy->html_type === 'checkbox') {
            if ($termSelected) {
                unset($termValues[ $termKey ]);
            } else {
                array_push($termValues, $termSlug);
            }
        } else {
            $termValues = [$termSlug];
        }

        $termValues = array_filter($termValues, function ($item) {
            return $item !== '';
        });

        $link = $this->getCurrentUrl($taxonomyNameClear);

        if (count($termValues) > 0) {
            $link = add_query_arg($taxonomyFilter, implode(',', $termValues), $link);
            $link = add_query_arg($taxonomyQueryType, $taxonomy->filter_query_type, $link);
        }

        return esc_url($link);
    }

    /**
     * Ordered terms for attribute taxonomy
     *
     * @param string $taxonomyName
     * @param string $orderBy
     *
     * @return array
     */
    private function getAttributeTerms($taxonomyName, $orderBy)
    {
        $query = ['taxonomy' => $taxonomyName];

        if (in_array($orderBy, ['id', 'name'])) {
            $query['orderby']    = $orderBy;
            $query['menu_order'] = false;
        }

        $taxonomyTerms = get_terms($query);

        if ($taxonomyTerms instanceof WP_Error) {
            return [];
        }

        $taxonomyTerms = $this->orderAttributeTerms($orderBy, $taxonomyTerms);

        return $taxonomyTerms;
    }

    /**
     * Order terms by taxonomy settings
     *
     * @param string $orderBy
     * @param array $taxonomyTerms
     *
     * @return array
     */
    private function orderAttributeTerms($orderBy, $taxonomyTerms)
    {
        if (in_array($orderBy, ['parent', 'name_num'])) {
            $order = [
                'parent'   => function ($a, $b) {
                    if ($a->parent == $b->parent) {
                        return 0;
                    }

                    return ($a->parent < $b->parent)? - 1 : 1;
                },
                'name_num' => function ($a, $b) {
                    $floatA = (float)$a->name;
                    $floatB = (float)$b->name;

                    if ($floatA == $floatB) {
                        return 0;
                    }

                    return ($floatA < $floatB)? - 1 : 1;
                },
            ];

            usort($taxonomyTerms, $order[ $orderBy ]);
        }

        return $taxonomyTerms;
    }

    /**
     * Ordered terms for brand taxonomy
     *
     * @param string $taxonomyName
     * @param string $orderBy
     *
     * @return array
     */
    private function getBrandTerms($taxonomyName, $orderBy)
    {
        $brandIds = array_keys($this->brandSettings);

        $terms = [];

        if (count($brandIds)) {
            $query['taxonomy'] = $taxonomyName;
            $query['orderby']  = $orderBy;
            $query['include']  = implode(',', $brandIds);

            $terms = get_terms($query);
        }

        return $terms instanceof WP_Error? [] : $terms;
    }


    /**
     * Add terms for given taxonomies and products for each term
     *
     * @param array $taxonomies
     *
     * @param $terms
     *
     * @return array
     */
    public function addTaxonomyTerms($taxonomies, $terms)
    {
        foreach ($taxonomies as $taxonomy) {
            $taxonomy->terms = [];
            if (isset($terms[ $taxonomy->traxonomy_name ])) {
                $taxonomyTerms = $terms[ $taxonomy->traxonomy_name ];
                foreach ($taxonomyTerms as $term) {
                    $taxonomy->terms[ $term->slug ] = $term;
                }
            }
        }

        return $taxonomies;
    }

    /**
     * Add product ids to each taxonomy terms
     *
     * @param array $taxonomies
     *
     * @return array
     */
    public function addTermProducts(array $taxonomies)
    {
        $term           = get_queried_object();
        $termTaxonomyId = $term instanceof WP_Term? $term->term_taxonomy_id : null;

        $termProducts = $this->getTermProductIdsByTaxonomy(array_keys($taxonomies), $termTaxonomyId);


        foreach ($taxonomies as $taxonomy) {
            foreach ($taxonomy->terms as $term) {
                if (isset($termProducts[ $term->term_id ])) {
                    $term->products = $termProducts[ $term->term_id ];
                } else {
                    $term->products = [];
                }
            }
        }

        return $taxonomies;
    }

    /**
     * Get product ids for each term
     *
     * @param $attributes
     * @param null|int $categoryId current category
     *
     * @return array
     */
    private function getTermProductIdsByTaxonomy($attributes, $categoryId = null)
    {
        if ($ids = $this->cache->get('attributes_' . $categoryId)) {
            return $ids;
        }

        global $wpdb;

        $ids = [];

        $query[] = 'SELECT DISTINCT t.term_id, r.object_id';
        $query[] = "FROM {$wpdb->term_relationships} r";
        $query[] = "INNER JOIN {$wpdb->term_taxonomy} t ON r.term_taxonomy_id = t.term_taxonomy_id";

        if ($categoryId) {
            $query[] = "INNER JOIN {$wpdb->term_relationships} tr ON tr.object_id = r.object_id AND tr.term_taxonomy_id = {$categoryId}";
        }

        $query[] = "WHERE t.taxonomy in  ('" . implode("','", $attributes) . "')";

        $query   = implode(' ', $query);
        $results = $wpdb->get_results($query, ARRAY_A);
        foreach ($results as $key => $result) {
            $ids[ $result['term_id'] ][] = $result['object_id'];
        }


        $this->cache->set('attributes_' . $categoryId, $ids);

        return $ids;
    }

    /**
     * Get current page url except $taxonomy args (filter_$taxonomy || query_type_$taxonomy)
     *
     * @param $taxonomyName
     *
     * @return string
     */
    protected function getCurrentUrl($taxonomyName)
    {
        global $wp;

        $link = home_url($wp->request);
        $pos  = strpos($link, '/page');

        if ($pos !== false) {
            $link = substr($link, 0, $pos);
        }


        foreach ($_GET as $key => $value) {
            $isCurrentTaxonomy = in_array($key, ['filter_' . $taxonomyName, 'query_type_' . $taxonomyName]);
            ;

            if (!$isCurrentTaxonomy) {
                $link = add_query_arg($key, wc_clean($value), $link);
            }
        }

        return $link;
    }

    /**
     * Type of attribute html element
     *
     * @param $id
     *
     * @return mixed
     */
    private function getHtmlType($id)
    {
        $htmlType = $this->attributeSettings[ $id ]['type'];

        return $htmlType;
    }

    /**
     * Type of query for filter
     *
     * @param string $htmlType
     *
     * @return string
     */
    private function getQueryType($htmlType)
    {
        switch ($htmlType) {
            case 'radio':
            case 'select':
                return 'and';
            case 'checkbox':
            default:
                return 'or';
        }
    }

    /**
     * Get real taxonomy name with pa_ prefix for woocommerce attributes
     *
     * @param stdClass $attribute
     *
     * @return string
     */
    private function getTaxonomyName(stdClass $attribute)
    {
        $prefix = isset($attribute->isCustomAttribute)? '' : 'pa_';

        $taxonomyName = $prefix . $attribute->attribute_name;

        return $taxonomyName;
    }


    /**
     * Get attributes sorted by settings
     *
     * @return array
     */
    private function getSortedAttributes()
    {
        $attributeTaxonomies = wc_get_attribute_taxonomies();

        if (taxonomy_exists(FilterPlugin::BRAND_TAXONOMY)) {
            $attributeTaxonomies[] = $this->getBrandTaxonomyAsAttribute();
        }

        //		filter attributes by settings
        $attributes = array_filter($attributeTaxonomies, function ($attribute) {
            return array_key_exists($attribute->attribute_id, $this->attributeSettings);
        });


        $byId = [];
        foreach ($attributes as $item) {
            $byId[ $item->attribute_id ] = $item;
        }

        //		order attributes by settings
        $attributes = array_replace($this->attributeSettings, $byId);

        foreach ($attributes as $key => $attribute) {
            if (!$attribute instanceof stdClass) {
                unset($attributes[ $key ]);
            }
        }


        return $attributes;
    }

    /**
     * Returns brand in woocommerce attribute format
     *
     * @return object
     */
    private function getBrandTaxonomyAsAttribute()
    {
        $brandTaxonomy = get_taxonomy(FilterPlugin::BRAND_TAXONOMY);

        $brandAttribute = (object)[
            'attribute_id'      => $brandTaxonomy->name,
            'attribute_name'    => $brandTaxonomy->name,
            'attribute_label'   => $brandTaxonomy->label,
            'attribute_orderby' => 'include',
            'isCustomAttribute' => true,
        ];

        return $brandAttribute;
    }

    /**
     * Main query to term relations table
     *
     * @param $taxonomies
     *
     * @return array
     */
    private function getTaxQuerySql($taxonomies)
    {
        global $wpdb;

        $taxQuery = WC_Query::get_main_tax_query();

        foreach ($taxQuery as $key => $query) {
            if (is_array($query) && in_array($query['taxonomy'], $taxonomies)) {
                unset($taxQuery[ $key ]);
            }
        }


        $taxQuery = new WP_Tax_Query($taxQuery);

        $taxQuerySql = $taxQuery->get_sql($wpdb->posts, 'ID');

        return $taxQuerySql;
    }

    /**
     * Main query to post meta table
     *
     * @return array|false
     */
    private function getMetaQuerySql()
    {
        global $wpdb;

        $meta_query     = WC_Query::get_main_meta_query();
        $meta_query     = new WP_Meta_Query($meta_query);
        $meta_query_sql = $meta_query->get_sql('post', $wpdb->posts, 'ID');

        return $meta_query_sql;
    }
}
