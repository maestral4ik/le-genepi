<?php namespace Premmerce\Filter\Filter;

use Premmerce\Filter\FilterPlugin;
use WC_Query;

class IntersectFilter{

	/**
	 * @var string
	 */
	private $searchQuery;

	/**
	 * @var FilterModel
	 */
	private $model;

	/**
	 * IntersectFilter constructor.
	 */
	public function __construct(){
		$this->model = new FilterModel(new FilterCache());
	}

	/**
	 * Clear filter cache
	 */
	public function clearCache(){
		$this->model->clearCache();
	}

	/**
	 * Warm up cache files
	 *
	 */
	public function warmUpCache(){
		$this->model->warmUpCache();
	}

	/**
	 * Add brand to woocommerce taxonomy query
	 *
	 * @param array $taxQuery
	 *
	 * @return array
	 */
	public function onWooCommerceTaxQuery($taxQuery){
		if(isset($_GET['filter_product_brand']) && $_GET['filter_product_brand']){
			$brand     = wc_clean($_GET['filter_product_brand']);
			$queryType = isset($_GET['query_type_product_brand'])? wc_clean($_GET['query_type_product_brand']) : 'or';

			$brand = [
				"taxonomy"         => FilterPlugin::BRAND_TAXONOMY,
				"field"            => "slug",
				"terms"            => explode(',', $brand),
				"operator"         => strtolower($queryType) == 'or'? 'in' : "and",
				"include_children" => false,
			];

			array_unshift($taxQuery, $brand);
		}


		return $taxQuery;
	}

	/**
	 * Handle posts_search hook to store search query
	 *
	 * @param $searchQuery
	 *
	 * @return mixed
	 */
	public function onPostsSearch($searchQuery){
		$this->searchQuery = $searchQuery;

		return $searchQuery;
	}

	/**
	 * Returns filter items
	 *
	 * @param array $settings
	 *
	 * @return array
	 */
	public function getItems($settings){
		$attributes = $this->model->getAttributes();

		$terms = $this->model->getTerms($attributes);

		$attributes = $this->model->addTaxonomyTerms($attributes, $terms);

		$attributes = $this->model->addTermProducts($attributes);

		$attributes = $this->countProductsForAttributes($attributes, $settings);

		return $attributes;
	}

	/**
	 * Count product for each attribute terms for current query
	 *
	 * @param array $attributes
	 * @param array $settings
	 *
	 * @return array
	 */
	private function countProductsForAttributes(array $attributes, array $settings){
		$queriedTaxonomyProducts = $this->getQueriedTaxonomyProducts($attributes);

		$queriedProducts = $this->model->getProductIdsByMainQuery(array_keys($attributes), $this->searchQuery);

		$hideEmpty = isset($settings['hide_empty'])? $settings['hide_empty'] : false;

		foreach($attributes as $key => $attribute){
			foreach($attribute->terms as $slug => $term){
				$term->count = $this->getTermCount($term, $queriedProducts, $queriedTaxonomyProducts, $attribute);

				if($hideEmpty && $term->count < 1){
					unset($attributes[ $key ]->terms[ $slug ]);
				}
			}

			if(count($attribute->terms) < 1){
				unset($attributes[ $key ]);
			}
		}

		return $attributes;
	}

	/**
	 * Get product ids for selected taxonomy terms
	 *
	 * @param $taxonomies
	 *
	 * @return array
	 */
	private function getQueriedTaxonomyProducts($taxonomies){
		$taxQuery = WC_Query::get_main_tax_query();

		$taxonomyProducts = [];
		foreach($taxQuery as $key => $item){
			if(is_array($item) && array_key_exists($item['taxonomy'], $taxonomies)){
				$taxonomyName = $item['taxonomy'];

				$taxonomy = $taxonomies[ $taxonomyName ];

				$taxonomyTerms = $taxonomies[ $taxonomyName ]->terms;

				$products = null;

				foreach($item['terms'] as $term){
					$termProducts = isset($taxonomyTerms[ $term ]->products)? $taxonomyTerms[ $term ]->products : [];

					if($term && is_array($termProducts)){
						if(is_null($products)){
							$products = $termProducts;
						}elseif($taxonomy->filter_query_type == 'or'){
							$products = array_merge($products, $termProducts);
						}else{
							$products = array_intersect($products, $termProducts);
						}
					}
				}

				$taxonomyProducts[ $taxonomyName ] = $products;
			}
		}


		return $taxonomyProducts;
	}

	/**
	 * @param object $term
	 * @param array $queriedProducts - products by main query
	 *
	 * @param array $queriedTaxonomyProducts - products by selected taxonomy terms
	 * @param $attribute
	 *
	 * @return int
	 * @internal param $queryType
	 *
	 */
	private function getTermCount($term, $queriedProducts, $queriedTaxonomyProducts, $attribute){
		if(!is_array($term->products) || !is_array($queriedProducts)){
			return 0;
		}

		$products = array_intersect($term->products, $queriedProducts);

		if(!count($products)){
			return 0;
		}

		if(!count($queriedTaxonomyProducts)){
			return count($products);
		}


		foreach($queriedTaxonomyProducts as $taxonomy => $taxonomyProducts){

			if($taxonomy !== $term->taxonomy){
				$products = array_intersect($products, $taxonomyProducts);
			}
		}

		return count($products);
	}
}
