<?php

use Premmerce\Filter\FilterPlugin;

/**
 * @var array $settings
 */

?>

<h2><?php _e( 'Settings', FilterPlugin::DOMAIN ) ?></h2>

<form action="<?php echo admin_url( 'admin-post.php' ) ?>" method="post">
    <input type="hidden" name="action" value="premmerce_filter_save_settings">

    <table class="form-table">
        <tbody>
        <tr>
            <th><?php _e( 'Behavior', FilterPlugin::DOMAIN ) ?>
            </th>
            <td>
                <fieldset>
                    <label>
						<?php $checked = ( isset( $settings['hide_empty'] ) && $settings['hide_empty'] ) ? 'checked' : '' ?>
                        <input type="checkbox" <?php echo $checked ?>
                               name="<?php echo FilterPlugin::OPTION_SETTINGS ?>[hide_empty]"
                               value="1">
						<?php _e( 'Hide empty terms', FilterPlugin::DOMAIN ) ?>
                    </label>
                </fieldset>
            </td>
        </tr>
        <tr>
            <th><?php _e( 'Show filter on pages', FilterPlugin::DOMAIN ) ?></th>
            <td>
                <fieldset>
                    <label>
						<?php $checked = ( isset( $settings['product_cat'] ) && $settings['product_cat'] ) ? 'checked' : '' ?>
                        <input type="checkbox" <?php echo $checked ?>
                               name="<?php echo FilterPlugin::OPTION_SETTINGS ?>[product_cat]" value="1">

						<?php _e( 'Product category', FilterPlugin::DOMAIN ) ?>
                    </label>
                    <br>
                    <label>
						<?php $checked = ( isset( $settings['tag'] ) && $settings['tag'] ) ? 'checked' : '' ?>
                        <input type="checkbox" <?php echo $checked ?>
                               name="<?php echo FilterPlugin::OPTION_SETTINGS ?>[tag]"
                               value="1">
						<?php _e( 'Tag', FilterPlugin::DOMAIN ) ?>
                    </label>
                    <br>
					<?php if ( taxonomy_exists( FilterPlugin::BRAND_TAXONOMY ) ): ?>

                        <label>
							<?php $checked = ( isset( $settings['product_brand'] ) && $settings['product_brand'] ) ? 'checked' : '' ?>
                            <input type="checkbox" <?php echo $checked ?>
                                   name="<?php echo FilterPlugin::OPTION_SETTINGS ?>[product_brand]" value="1">
							<?php _e( 'Brand', FilterPlugin::DOMAIN ) ?>
                        </label>
                        <br>
					<?php endif; ?>
                    <label>
						<?php $checked = ( isset( $settings['search'] ) && $settings['search'] ) ? 'checked' : '' ?>
                        <input type="checkbox" <?php echo $checked ?>
                               name="<?php echo FilterPlugin::OPTION_SETTINGS ?>[search]"
                               value="1">
						<?php _e( 'Search', FilterPlugin::DOMAIN ) ?>
                    </label>
                    <br>
                    <label>
						<?php $checked = ( isset( $settings['shop'] ) && $settings['shop'] ) ? 'checked' : '' ?>
                        <input type="checkbox" <?php echo $checked ?>
                               name="<?php echo FilterPlugin::OPTION_SETTINGS ?>[shop]" value="1">
						<?php _e( 'Store', FilterPlugin::DOMAIN ) ?>
                    </label>
                    <br>
                </fieldset>


            </td>
        </tr>
        </tbody>
    </table>
	<?php submit_button() ?>
</form>
<h2><?php _e( 'Cache', FilterPlugin::DOMAIN ) ?></h2>
<table>
    <tbody>
    <tr>
        <td>
            <form action="<?php echo admin_url( 'admin-post.php' ) ?>" method="post">
                <button class="button"><?php _e( 'Clear cache', FilterPlugin::DOMAIN ) ?></button>
                <input type="hidden" name="action" value="premmerce_filter_cache_warmup">
            </form>
        </td>
    </tr>
    </tbody>
</table>

