<?php use Premmerce\Filter\FilterPlugin; ?>
<div class="alignleft actions bulkactions">
    <label for="bulk-action-selector-top" class="screen-reader-text"><?php _e('Select bulk action', FilterPlugin::DOMAIN) ?></label>
    <select data-bulk-action-select>
		<?php foreach($actions as $key => $action): ?>
            <option value="<?php echo $key ?>"><?php echo $action ?></option>
		<?php endforeach; ?>
    </select>
    <button type="button" data-action="<?php echo $dataAction?>"
            class="button"><?php _e('Apply', FilterPlugin::DOMAIN) ?></button>
</div>