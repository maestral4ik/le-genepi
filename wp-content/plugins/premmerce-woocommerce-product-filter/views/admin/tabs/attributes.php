<?php use Premmerce\Filter\FilterPlugin;

/**
 * @var array $attributes
 * @var array $attributesConfig
 * @var array $types
 * @var array $actions
 */
?>

<h2><?php _e('Attributes', FilterPlugin::DOMAIN) ?></h2>


<div class="tablenav top">
	<?php include __DIR__ . '/actions.php' ?>
</div>

<table class="widefat premmerce-filter-table">
    <thead>
    <tr>
        <td class="check-column">
            <label for="">
                <input type="checkbox" data-select-all="attribute">
            </label>
        </td>
        <th><?php _e('Display type', FilterPlugin::DOMAIN) ?></th>
        <th><?php _e('Attribute', FilterPlugin::DOMAIN) ?></th>
        <th class="premmerce-filter-table__align-center"><?php _e('Visibility', FilterPlugin::DOMAIN) ?></th>
        <th class="premmerce-filter-table__align-right"></th>
    </tr>
    </thead>
    <tbody data-sortable="premmerce_filter_sort_attributes">

	<?php if(count($attributes) > 0): ?>
		<?php foreach($attributes as $id => $label): ?>

            <tr>
                <td>
                    <input data-selectable="attribute" type="checkbox" data-id="<?php echo $id ?>">
                </td>
                <td>
                    <select data-single-action="premmerce_filter_bulk_action_attributes" data-id="<?php echo $id ?>">
						<?php foreach($types as $key => $type): ?>
                            <option <?php echo selected($key, $attributesConfig[ $id ]['type']) ?>
                                    value="<?php echo $key ?>"><?php echo $type ?></option>
						<?php endforeach; ?>
                    </select>
                </td>
                <td><?php echo $label ?></td>
                <td class="premmerce-filter-table__align-center">
					<?php $active = $attributesConfig[ $id ]['active'] ?>
                    <span title="<?php $active? _e('Displayed', FilterPlugin::DOMAIN) : _e('Hidden', FilterPlugin::DOMAIN) ?>"
                          class="dashicons dashicons-<?php echo $active? "visibility" : "hidden" ?>"></span>
                </td>
                <td class="premmerce-filter-table__align-right"><span data-sortable-handle
                                                                      class="sortable-handle dashicons dashicons-menu"></span>
                </td>
            </tr>
		<?php endforeach ?>
	<?php else: ?>
        <tr>
            <td colspan="5">
				<?php _e('No items found', FilterPlugin::DOMAIN) ?>
            </td>
        </tr>
	<?php endif ?>
    </tbody>
</table>
<div class="tablenav bottom">
	<?php include __DIR__ . '/actions.php' ?>
</div>
