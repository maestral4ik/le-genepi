<?php
if(function_exists('premmerce_pwpf_fs') && premmerce_pwpf_fs()->is_registered()){
	premmerce_pwpf_fs()->add_filter('hide_account_tabs', '__return_true');
	premmerce_pwpf_fs()->_account_page_load();
	premmerce_pwpf_fs()->_account_page_render();
}
