<?php use Premmerce\Filter\FilterPlugin; ?>
<div class="wrap">
    <h1><?php _e('Premmerce WooCommerce Product Filter', FilterPlugin::DOMAIN) ?></h1>
    <h2 class="nav-tab-wrapper">
		<?php foreach($tabs as $tab => $name): ?>
			<?php $class = ($tab == $current)? ' nav-tab-active' : ''; ?>
            <a class='nav-tab<?php echo $class ?>'
               href='?page=premmerce-filter-admin&tab=<?php echo $tab ?>'><?php echo $name ?></a>
		<?php endforeach; ?>
    </h2>

	<?php $file = __DIR__ . "/tabs/{$current}.php" ?>
	<?php if(array_key_exists($current, $tabs) && file_exists($file)): ?>
		<?php include $file ?>
	<?php endif; ?>

</div>

