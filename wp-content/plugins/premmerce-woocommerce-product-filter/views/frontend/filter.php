<?php
/**
 * @var array $attributes
 * @var array $args
 * @var string $title
 *
 * use instance['title'] to show widget title
 */
?>

<?php foreach ( $attributes as $attribute ): ?>

	<?php echo $args['before_widget']; ?>

	<?php echo $args['before_title'] . $attribute->attribute_label . $args['after_title'] ?>

	<?php if ( isset( $attribute->terms ) ) : ?>

		<?php if ( $attribute->html_type == 'select' ) : ?>
            <select onchange="window.location.assign(this.value);">
                <option value="<?php echo $attribute->reset_url ?>"><?php printf( __( 'Any %s', 'woocommerce' ), $attribute->attribute_label ) ?></option>
				<?php foreach ( $attribute->terms as $term ): ?>
					<?php $selected = $term->checked ? 'selected' : ''; ?>
                    <option <?php echo $selected ?>
                            value="<?php echo $term->link ?>"><?php echo $term->name . ' (' . ( $term->count ) . ')' ?></option>
				<?php endforeach ?>
            </select>
		<?php else: ?>
            <ul>
				<?php foreach ( $attribute->terms as $term ): ?>
                    <li>
                        <label>
                            <a href="<?php echo $term->link ?>">
								<?php if ( $attribute->html_type == 'checkbox' ) : ?>
                                    <input <?php if ( $term->checked ): ?>checked<?php endif ?> type="checkbox"
                                           onclick="this.parentElement.click()">
									<?php echo $term->name . ' (' . ( $term->count ) . ')' ?>
								<?php elseif ( $attribute->html_type == 'radio' ) : ?>
                                    <input <?php if ( $term->checked ): ?>checked<?php endif ?> type="radio"
                                           onclick="this.parentElement.click()">
									<?php echo $term->name . ' (' . ( $term->count ) . ')' ?>
								<?php endif; ?>
                            </a>
                        </label>
                    </li>
				<?php endforeach ?>
            </ul>
		<?php endif; ?>
        
	<?php endif ?>

	<?php echo $args['after_widget']; ?>

<?php endforeach ?>
