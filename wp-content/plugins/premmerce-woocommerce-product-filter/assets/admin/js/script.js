jQuery(function ($) {

    //fix column sizes on
    $('[data-sortable] td').each(function () {
        $(this).css('width', $(this).width() + 'px');
    });

    //follow main checkbox by selectable
    $('[data-select-all]').change(function () {
        var main = $(this);
        var name = main.data('select-all');
        $('[data-selectable="' + name + '"]').prop('checked', main.prop('checked'));
    });

    var table = $('[data-sortable]');

    table.sortable({
        handle: '[data-sortable-handle]',
        axis: "y",
        update: function () {
            var ids = [];
            var action = table.data('sortable');
            $('input[data-id]').each(function () {
                ids.push($(this).data('id'));
            });

            $.post(ajaxurl, {ids: ids, action: action}, function () {
                window.location.reload();
            });
        }
    });

    $('[data-single-action]').change(function () {
        var $this = $(this);

        update([$this.data('id')], $this.data('single-action'), $this.val())


    });

    $('button[data-action]').click(function () {
        var $this = $(this);

        var action = $this.data('action');
        var value = $this.parent('.bulkactions').find('[data-bulk-action-select]').val();

        var ids = [];
        $('input[data-id]:checked').each(function () {
            ids.push($(this).data('id'));
        });

        update(ids, action, value);

    });

    function update(ids, action, value) {
        $.post(ajaxurl, {ids: ids, action: action, value: value}, function () {
            window.location.reload();
        })
    }


});
