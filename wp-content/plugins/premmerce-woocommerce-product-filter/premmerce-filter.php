<?php

use Premmerce\Filter\FilterPlugin;

/**
 * Premmerce WooCommerce Product Filter
 *
 * @package           Premmerce\Filter
 *
 * @wordpress-plugin
 * Plugin Name:       Premmerce WooCommerce Product Filter
 * Plugin URI:        https://premmerce.com/woocommerce-product-filter/
 * Description:       Premmerce WooCommerce Product Filter plugin is a convenient and flexible tool for managing filters for WooCommerce products.
 * Version:           1.0.4
 * Author:            Premmerce
 * Author URI:        https://premmerce.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       premmerce-filter
 * Domain Path:       /languages
 *
 * WC requires at least: 3.0.0
 * WC tested up to: 3.3.0
 */

// If this file is called directly, abort.
if(!defined('WPINC')){
	die;
}

call_user_func(function(){

	require_once plugin_dir_path(__FILE__) . 'vendor/autoload.php';

	if(!get_option('premmerce_version')){
		require_once plugin_dir_path(__FILE__) . '/freemius.php';
	}

	$main = new FilterPlugin(__FILE__);

	register_activation_hook(__FILE__, [$main, 'activate']);

	register_deactivation_hook(__FILE__, [$main, 'deactivate']);

	register_uninstall_hook(__FILE__, [FilterPlugin::class, 'uninstall']);

	$main->run();
});