<?php
// Create a helper function for easy SDK access.
function premmerce_pwpf_fs(){
	global $premmerce_pwpf_fs;

	if(!isset($premmerce_pwpf_fs)){
		// Include Freemius SDK.
		require_once dirname(__FILE__) . '/freemius/start.php';

		$premmerce_pwpf_fs = fs_dynamic_init([
			'id'             => '1519',
			'slug'           => 'premmerce-woocommerce-product-filter',
			'type'           => 'plugin',
			'public_key'     => 'pk_20f16471b14ab029cbbc55d432950',
			'is_premium'     => false,
			'has_addons'     => false,
			'has_paid_plans' => false,
			'menu'           => [
				'slug'    => 'premmerce-filter-admin',
				'account' => false,
				'contact' => false,
				'support' => false,
				'parent'  => [
					'slug' => 'premmerce',
				],
			],
		]);
	}

	return $premmerce_pwpf_fs;
}

// Init Freemius.
premmerce_pwpf_fs();
// Signal that SDK was initiated.
do_action('premmerce_pwpf_fs_loaded');