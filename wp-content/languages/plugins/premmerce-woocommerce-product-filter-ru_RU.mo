��    &      L  5   |      P     Q     Y     `  	   f  
   p     {     �     �     �     �     �  
   �     �     �  	   �     �     �     �     �  	     $     |   5     �     �     �     �     �                     5     ;  .   ?     n  
   t       1   �  �  �     O     ^     l          �     �  
   �     �     �     �     �     �          .     L     e     t  .   �  $   �  	   �  $   �  �   	     �	  3   �	  !   
     <
  
   K
  2   V
     �
  9   �
     �
     �
  @   �
     -     @     S  1   i            
   !   %                           	                   #                       "               $                                                                  &          Account Any %s Apply Attribute Attributes Behavior Brand Brands Bulk Actions Cache Clear cache Contact Us Display Display type Displayed Hidden Hide Hide empty terms No items found Premmerce Premmerce WooCommerce Product Filter Premmerce WooCommerce Product Filter plugin is a convenient and flexible tool for managing filters for WooCommerce products. Premmerce filter Product attributes filter Product category Product filter Search Select bulk action Settings Show filter on pages Store Tag The %s plugin requires %s plugin to be active! Title Visibility https://premmerce.com https://premmerce.com/woocommerce-product-filter/ PO-Revision-Date: 2018-02-28 11:15:41+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: GlotPress/2.4.0-alpha
Language: ru
Project-Id-Version: Plugins - Premmerce WooCommerce Product Filter - Stable (latest release)
 Аккаунт Любой %s Применить Атрибут Атрибуты Поведение Бренд Бренды Действия Кэш Очистить кеш Свяжитесь с нами Отображать Тип отображения Отображается Скрытый Не отображать Спрятать пустые значения Значений не найдено Premmerce Premmerce WooCommerce Product Filter Данный плагин представляет собой удобный и гибкий инструмент для управления фильтрами для товаров WooCommerce. Premmerce filter Фильтр товаров по атрибутах Категория товаров Product filter Поиск Выберите массовое действие Настройки Показывать фильтр на страницах Магазин Тег Для работы плагина %s нужен плагин %s Заголовок Видимость https://premmerce.com https://premmerce.com/woocommerce-product-filter/ 