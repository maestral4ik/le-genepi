<?php
	
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<div class="panel filters">
	<?php dynamic_sidebar('products_menu') ?>
	<div class="filter-item filter-item-big">
		<a href="/product-category/products/" class="btn btn-default btn-blue">посмотреть все продукты</a>
	</div>	
</div>

<script>
function _change_category() {
    location.href=document.getElementById('category').options[document.getElementById('category').selectedIndex].value;
}
</script>