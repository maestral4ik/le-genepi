<?php // Template Name: Советы?>
<?php 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
get_header(); ?>
<main>
<div class='container container-white'>			
	<div class="panel panel-default">
		<div class="panel-body">
			<div class='row'>
				<div class='col-xs-4'>
					<div class='row'>
						<ul class="list-inline consult-social">
							<li class="comment-r">
								<a class="btn btn-social-icon btn-comments-o btn-lg btn-social-color"
									data-toggle="modal" data-target="#modal-consult">
									<i class="fa fa-comments-o"></i>
								</a>
							</li>
						</ul>
					</div>
					<div class='row consult-r'>
						<h4>Ваш личный On-line консультант</h4>
						<p><a id="myBtn" href="javascript:;" data-toggle="modal" data-target="#modal-consult">получить консультацию...</a></p>
					</div>
				</div>
				<div class='col-xs-4'>
					<div class='row'>
						<ul class="list-inline consult-social">
							<li class="comment">
								<a class="btn btn-social-icon btn-comments-o btn-lg btn-social-color"
									href="/advice/#faqs">
									<i class="fa fa-question "></i>
								</a>
							</li>
						</ul>
					</div>
					<div class='row consult'>
						<h4>Часто задаваемые вопросы</h4>
						<p></p>
					</div>
				</div>
				
				<div class='col-xs-4'>
					<div class='row'>
							<ul class="list-inline consult-social">
								<li class="soc-paket-f">
									<a class="btn btn-social-icon btn-facebook btn-lg btn-social-color"
										href="https://www.facebook.com/legenepi.ru"
										target="_blank">
										<i class="fa fa-facebook"></i>
									</a>
								</li>
								<li class="soc-paket-i">
									<a class="btn btn-social-icon btn-instagram btn-lg btn-social-color"
										href="http://instagram.com/le_genepi?ref=badge"
										target="_blank">
										<i class="fa fa-instagram"></i>
									</a>
								</li>
							</ul>	
					</div>
					<div class='row consult-l'>
						<h4>Кабинет le génépi Красота insideout</h4>
						<p>больше интересного о красоте и здоровье</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-body">
				<div class='row consult-top-p'>
					<p class='consult-top-first'>
						Для полноценного ухода за кожей необходимо
						использовать<br> весь комплекс средств по ухода за кожей.
					</p>
						
					<p>
						Залог красивой здоровой и молодой кожей &mdash;
						в регулярности и полноте ухода.
					</p>
					
					<p>
						Сколько ухода вы вложите в кожу,
						столько эффекта вы получите.
					</p>
						
					<p>
						Для того, чтобы в полной мере почувствовать эффект, качество и действенности
						<br> косметических средств <b>le génépi</b>, советуем вам не использовать косметические
						<br>средства других марок вместе со средствами <b>le génépi</b>.
					</p>	
				</div>
				<div class='row consult-slogan'>
					<h1><b>экологически чисто</b> <p class="bio-inline">bio</p>технологически совершенно</h1>
				</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-body">
			<?php print_sections(); ?>
			<div id="faq" class="row faq">
				<a id="faqs" class='anchor'></a>
				<h3>Часто задаваемые вопросы</h3>
			</div>
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="faq-heading-1">
				  		<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq-1" aria-expanded="false" aria-controls="faq-1">
					 			<h4>Почему у le génépi нет линий продуктов?</h4>
							</a>
					  	</h4>
					</div>
					<div id="faq-1" class="panel-collapse collapse faq-a" role="tabpanel" aria-labelledby="faq-heading-1">
						<div class="panel-body">
							<p>Марка le génépi исповедует новую концепцию создания продуктов. Средства марки не привязаны к какому-либо ингредиенту, составляющему концепцию линии, как это присходит у классических марок, известных сегодня. <br>Каждый продукт марки le génépi изначально создается для устранения той проблемы кожи, на которую он направлен. Его главная задача - максимально эффективно работать в области применения. И для этого, специалисты марки, используют самые действенные компоненты не ограничивая себя концепцией определенных линий.<br><br>Кроме того, это дает максимальную гибкость клиентам при выборе средств ухода за кожей, так как все продукты марки легко сочетаются между собой.</p>
						</div>
					</div>
			    </div>	
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="faq-heading-5">
						<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq-5" aria-expanded="false" aria-controls="faq-5">
							 	<h4>Как мне подобрать подходящее средство?</h4>
							</a>
						</h4>
					</div>
					<div id="faq-5" class="panel-collapse collapse faq-a" role="tabpanel" aria-labelledby="faq-heading-5">
				  		<div class="panel-body">
							<p><p>На нашем сайте существует несколько возможностей, с помощью которых, не отходя от своего компьютера, вы сможете подобрать себе средство максимально отвечающее вашим нуждам.</p>
							<ol>
								<li> "Он-лайн консультант" на странице Cоветы. Это форма, с помощью которой вы можете задать любой вопрос главному косметологу компании le génépi в России и получить исчерпывающий ответ с персональными рекомендациями.</li>
								<li> Наша новейшая система подбора средств "Он-лайн подбор". Ответив на несколько простых вопросов вы получите персональные рекомендациии по уходу.</li>
								<li>На нашей странице facebook Кабинет le génépi. Красота InsideOut можно найти дополнительную информацию по уходу и средствам le génépi.</li>
							</ol></p>
						</div>
					</div>
				</div>	
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="faq-heading-2">
				  		<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq-2" aria-expanded="false" aria-controls="faq-2">
					 			<h4> le génépi это профессиональная косметика?</h4>
							</a>
			  			</h4>
					</div>
					<div id="faq-2" class="panel-collapse collapse faq-a" role="tabpanel" aria-labelledby="faq-heading-2">
						<div class="panel-body">
							<p>Понятие профессиональная косметика, возникло в результате рекламного хода, некоторых производителей косметики (в основном низкокачественной), которые стали писать professional на упаковке, чтобы привлечь покупателей.<br>В настоящее время, у косметологов профессионалов нет косметики, которая не была бы доступна широкой публике т.к. любая "профессиональная", или правильно сказать, Кабинетная косметика, имеет линии для домашнего ухода.<br>Основное отличие косметики, которой пользуются профессиональные косметологи, это объем упаковки. При этом, справеливо отметить, что некоторые средства, которые не удобно или не безопасно использовать в домашних условиях, включаются только в кабинетные линии марок.<br>Мы создали le génépi для "домашних профессионалов".</p>
						</div>
					</div>
			  	</div>	
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="faq-heading-3">
				  		<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq-3" aria-expanded="false" aria-controls="faq-3">
					 			<h4>Бывают ли случаи аллергических реакций на продукцию?</h4>
							</a>
				  		</h4>
					</div>
					<div id="faq-3" class="panel-collapse collapse faq-a" role="tabpanel" aria-labelledby="faq-heading-3">
				  		<div class="panel-body">
							<p>Косметические средства le genepi гипоаллергенны и не вызывают аллергических реакций.<br>Продукты марки содержат только компоненты самого высокого качества, а все природные экстракты обладают сертификатами Ecocert и собираются с соблюдением правил Ответственного производства EFT (Ecocert fair trade in the spirit of solidarity and responsibility).<br><br>Если вы чувствуете действие косметики le genepi, то только потому, что концентрация активных ингредиентов в любом продукте марки - максимальна. Поэтому, любые проявления вроде пощипывания или легкого покраснения - это показатель реального действия косметического средства на кожу.<br><br>Пожалуйста, воспользуйтесь "он-лайн консультантом" в разделе Советы, чтобы обратиться за консультацией к косметологу марки le gènèpi в России.</p>
				  		</div>
					</div>
				</div>	
			
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="faq-heading-4">
				  		<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq-4" aria-expanded="false" aria-controls="faq-4">
					 			<h4>Можно ли использовать косметику le génépi мужчинам?</h4>
							</a>
				 		</h4>
					</div>
					<div id="faq-4" class="panel-collapse collapse faq-a" role="tabpanel" aria-labelledby="faq-heading-4">
				  		<div class="panel-body">
							<p>Да, все косметические средства, подходят и мужчинам.<br>Особенно мы советуем обратить внимание мужчин на:
							<ul>
								<li>Средство для умывания - organic wave face deep cleanser органический гель
								для глубокого очищения кожи лица.</li>
								<li> Крем, который можно использовать, как крем после бритья - comfortech face cream
								High-tech уход для чувствительной кожи.</li>
								<li> Увлажняющий, восстанавливающий и регенерирующий крем mediterranean natural face cream подходит для всей семьи, в том числе для детей.</li>
								<li> Волшебный Крем для рук, который легко впитывается и не оставляет следов.</li>
								<li>Любое другое средство le génépi.</li>
							</ul></p>
						</div>
					</div>
				</div>	
			</div>	
		</div>
	</div>
</div></a>
			
</div>
</main>

<!-- The Modal -->
<div id="myModal" class="modal">
  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <p>Some text in the Modal..</p>
    	<?php echo do_shortcode( '[contact-form-7 id="265" title="Индивидуальный подбор средств"]' ); ?>
  </div>
</div>

<script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


</script>


<?php get_footer(); ?>
