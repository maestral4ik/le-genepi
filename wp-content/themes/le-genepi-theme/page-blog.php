<?php 
/*
    Template Name: Blog
*/
?>
<?php get_header(); ?>
<main>
<div class='container container-white'>         
    <div class="panel panel-default">
        <div class="panel-body blog">
            <?php // Display blog posts on any page @ http://m0n.co/l
            $temp = $wp_query; $wp_query= null;
            $wp_query = new WP_Query(); $wp_query->query('showposts=5' . '&paged='.$paged);
            while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <div class='row'>               
                <div class="col-xs-3">
                    <a href="<?php the_permalink(); ?>"><img class="blog-title-pic" src="<?php the_post_thumbnail_url('medium'); ?>" alt="<?php the_title(); ?>"></a>
                </div>
            <div class="col-xs-9">
                <div class="row">
                    <div class="col-xs-12 post-title">
                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                        <p><?php echo get_the_date('j-n-Y'); ?></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <p><?php the_excerpt(); ?></p>
                        <a href="<?php the_permalink(); ?>"> Читать далее... </a>
                    </div>
                </div>
            </div>
        </div>
            <hr class="blog-sep">
            <?php endwhile; ?>
        <?php if ($paged > 1) { ?>
        <nav id="nav-posts">
            <div class="prev"><?php next_posts_link('&laquo; Назад'); ?></div>
            <div class="next"><?php previous_posts_link('Вперед &raquo;'); ?></div>
        </nav>
        <?php } else { ?>
        <nav id="nav-posts">
            <div class="prev"><?php next_posts_link('&laquo; Назад'); ?></div>
        </nav>
        <?php } ?>
        <?php wp_reset_postdata(); ?>
    </div>
</div>
</main> 
 
 
 
 
 
 
<?php get_footer(); ?>

