<?php 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header(); ?>
<main>
<?php print_sections(); ?>
<?php if(is_cart()):?>

<div role="page" class="container-flex">
	

<?php else :?>

<?php endif; ?>


		<?php while(have_posts()): the_post() ?>
		
			<?php if(is_cart() || is_checkout()) :?>
			
				<?php get_template_part( 'template-parts/content', 'page');?>
			
			<?php endif; ?>
		
		<?php endwhile;?>



	
</div>
</main>
<?php get_footer(); ?>