<footer>
      

<div class="container-flex">
  <div class="footer-body">
    <?php wp_nav_menu(array(
                  'theme_location' => 'bottom',
                  'container'      => 'false',
                  'items_wrap' => '%3$s',
                  'walker' => new Custom_Walker_Battom_Menu()
                  )); ?>
    <div class="footer-item social">
      <p>Кабинет le génépi</p>
      <div class="links">
        <a class="btn btn-default" href="https://www.facebook.com/legenepi.ru" target="_blank"><i class="fa fa-facebook"></i></a>
        <a class="btn btn-default" href="http://instagram.com/le_genepi?ref=badge" target="_blank"><i class="fa fa-instagram"></i></a>
      </div>
      <div><img src="<?php bloginfo('template_url'); ?>/assets/img/partners/visa-mastercard.png" alt="visa mastercard logo"></div>
    </div>
  </div>
  <div class="copyright">
    <p>&copy; le-genepi.com </p>
  </div>
</div>

    </footer>