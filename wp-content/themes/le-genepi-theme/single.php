<?php get_header(); ?>
<?php 
$next_post = get_next_post();
$previous_post = get_previous_post();
?>
<main>
<div class='container container-white'>			
	<div class="panel panel-default">
		<div class="panel-body post-title-pic-cont">
			
			<?php while( have_posts() ) : the_post();
                        $more = 1;?> 
			<div class="row">
				<div class="post-title-pic" style="background-image:url(<?php the_post_thumbnail_url('large'); ?>)">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 post-padd">
					<h3><?php the_title(); ?></h3>
				</div>
			</div>
			<div class="row post-body">
				<div class="col-xs-12 post-padd">
				  <?php the_content(); ?>
				</div>
			</div>
			<hr>
			<div class="row prev_next">
				<?php if( !empty($previous_post) ): ?>
				<div class="col-xs-2 previous_post-class">
					<a href="<?php echo get_permalink( $previous_post->ID ); ?>">Предыдущая запись</a>
				</div>
				<?php endif; ?>
				<?php if( !empty($next_post) ): ?>
				<div class="col-xs-2 col-xs-offset-8 next-post">
					<a href="<?php echo get_permalink( $next_post->ID ); ?>">Следующая запись</a>
				</div>
				<?php endif;?>
				<div class="col-xs-2 col-xs-offset-8 next-post">
				</div>
			</div>
                    <?php endwhile; ?>
		</div>
	</div>
</div>
</main>
<?php get_footer(); ?>