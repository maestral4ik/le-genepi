<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

if(empty($product) || !$product->is_visible())
{
	return;
}

	// <div class="shop-product">
	// 	<div>
	// 		<h4><a href="http://yr-trarcv.pbz.r.ru.a2ip.ru/products/cleansers/ref.AT.1998" target="_blank">органический очищающий гель</a></h4>
	// 		<h5 itemprop="alternateName">органический гель<br>для глубокого очищения кожи лица</h5>
	// 	</div>
	// 	<img src="./Online-магазин _ Le génépi_files/cleancer_opisanie_1.jpg" alt="органический очищающий гель">
		
	// 	<div>
	// 		<p class="storage-green"><i class="fa fa-check"></i> в наличии</p>
	// 		<p><i class="fa fa-truck"></i> доставка возможна 10&nbsp;марта</p>
	// 	</div>
	// 	<div class="shop-product-actions">
	// 		<span class=""><b>2600</b> <i class="fa fa-rub"></i></span><button role="modal" data-target="#modal-cart" onclick="add_position(&quot;ref.AT.1998&quot;);" type="button" class="btn btn-default">в корзину</button><a href="http://yr-trarcv.pbz.r.ru.a2ip.ru/products/cleansers/ref.AT.1998" type="button" class="btn btn-default ">подробнее</a>
	// 	</div>
		
	// </div>
?>

<div class="shop-product">
	<?php //do_action('woocommerce_before_shop_loop_item'); ?>
	<?php do_action('woocommerce_before_shop_loop_item_title'); ?>

	<?php //do_action('woocommerce_shop_loop_item_title'); ?>
	<?php //do_action('woocommerce_after_shop_loop_item_title'); ?>
	<?php do_action('woocommerce_after_shop_loop_item'); ?>
</div>