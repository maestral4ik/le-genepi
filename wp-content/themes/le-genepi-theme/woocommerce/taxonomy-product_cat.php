<?php
	
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header();

?>


<main>
			
<div class="container-flex">
	<div class="panel">
		<h2 id="filter-name"><?php woocommerce_page_title(); ?> // Все продукты</h2>
	</div>



 <?php wc_get_template_part('products', 'menu'); ?>

<div class="panel" id="shop-body">
<?php if(have_posts()) : ?>
<?php while(have_posts()) : the_post();?>
	<?php wc_get_template_part('content', 'product');?>
<?php endwhile; ?>
<?php else :?>
	<?php wc_get_template('loop/no-products-found.php'); ?>
<?php endif;?>
</div>

<div role="page" class="container-flex">
	<div class="panel cosmetolog">
		
	<h2 class="blue">Индивидуальный подбор средств</h2>
	<form class="cosmetolog-form -visor-no-click" role="cosmetolog" method="POST">
		<input type="hidden" name="csrfmiddlewaretoken" value="XUH3boUrUyg5z70c7kg8KRaX3M61MtRS">
		<p class="sub-text">Заполните заявку и врач-косметолог le&nbsp;génépi подберет вам индивидуальный уход.</p>
		<div class="label">
			<label for="id_name">Имя</label>
		</div>
		<div class="field">
			<input type="text" id="id_name" name="name" placeholder="Екатерина" required="">
		</div>
		<p class="sub-text">Выберите предпочтительный вид связи</p>
		<div class="label">
			<label for="id_email_c">Почта</label>
		</div>
		<div class="field field-choise">
			<span><input onclick="document.getElementById(&#39;id_email_c&#39;).disabled = false; document.getElementById(&#39;id_phone&#39;).disabled = true;" type="radio" data-id="#id_email" name="form_option" checked=""></span>
			<input type="email" id="id_email_c" name="email" placeholder="any@email.com" required="">
		</div>
		<div class="label">
			<label for="id_phone">Телефон</label>
		</div>
		<div class="field field-choise">
			<span><input onclick="document.getElementById(&#39;id_phone&#39;).disabled = false; document.getElementById(&#39;id_email_c&#39;).disabled = true;" type="radio" data-id="#id_phone" name="form_option"></span>
			<input type="tel" id="id_phone" name="phone" placeholder="+7 (___) ___-__-__" disabled="" required="">
		</div>
		<p class="sub-text"></p>
		<div class="label">
			<label for="id_msg">Сообщение</label>
		</div>
		<div class="field">
			<textarea id="id_msg" name="msg" class="form-control" placeholder="" required=""></textarea>
		</div>
		<div class="submit">
			<button type="submit">Получить консультацию</button>
		</div>		
	</form>

	</div>
</div>


<div class="container-flex">
	<div class="panel shipping tabs">
		<div class="tabs-header">
			
				<a role="tab" href="http://yr-trarcv.pbz.r.ru.a2ip.ru/ru/shop/products/#1" class=" active ">
					<i class="fa fa-truck"></i>
					<span>Доставка</span>
				</a>
			
				<a role="tab" href="http://yr-trarcv.pbz.r.ru.a2ip.ru/ru/shop/products/#2" class="">
					<i class="fa fa-rub"></i>
					<span>Оплата</span>
				</a>
			
				<a role="tab" href="http://yr-trarcv.pbz.r.ru.a2ip.ru/ru/shop/products/#4" class="">
					<i class="fa fa-percent"></i>
					<span>Промо-код</span>
				</a>
			
				<a role="tab" href="http://yr-trarcv.pbz.r.ru.a2ip.ru/ru/shop/products/#3" class="">
					<i class="fa fa-repeat"></i>
					<span>Возврат</span>
				</a>
			
				<a role="tab" href="http://yr-trarcv.pbz.r.ru.a2ip.ru/ru/shop/products/#5" class="">
					<i class="fa fa-phone"></i>
					<span>Консультация</span>
				</a>
			
				<a role="tab" href="http://yr-trarcv.pbz.r.ru.a2ip.ru/ru/shop/products/#6" class="">
					<i class="fa fa-file-pdf-o"></i>
					<span>Сертификат</span>
				</a>
			
		</div>
		<div class="tabs-body">
			
				<div role="tab-body" id="1" class=" active ">
					<h3><normal class="lgnp-blue">Бесплатная доставка</normal> по всей России  для всех клиентов le&nbsp;génépi.</h3>
<p>Доставку осуществляет курьерская служба DPD <a href="http://qcq.eh.r.ru.a2ip.ru/" target="_blank">dpd.ru</a>. Оформите заказ на сайте <a href="http://yr-trarcv.pbz.r.ru.a2ip.ru/" target="_blank">le-genepi.com</a>, укажите свои контактные данные и номер мобильного телефона. Нам потребуется 24 часа (в рабочие дни), чтобы сформировать ваш заказ и передать его в курьерскую службу для отправки. </p>
<p>Если у вас возникли вопросы по доставке или оплате, свяжитесь с нами по<br>
e-mail <a href="mailto:orders@le-genepi.com">orders@le-genepi.com</a><br>
тел. <a href="tel:+78129293883">+7 (812) 929 38 83</a>.<br>
# Закажите звонок и мы вам перезвоним</p>
<p>Или укажите свой вопрос в комментариях к заказу. Наш менеджер свяжется с вами, чтобы уточнить детали заказа и ответит на все интересующие вас вопросы.</p>
<p>Доставка занимает от 1 до 5 дней в зависимости от удаленности пункта назначения, погодных условий и прочих факторов от нас не зависящих.</p>
<p>Как только мы передадим отправление в службу доставки, вы получите смс-уведомление с номером вашего заказа.
<br>Вы сможете отслеживать доставку вашего заказа по номеру на сайте DPD.</p>		
<p>Более того, получив номер доставки, на сайте курьерской службы, вы сможете изменить время, дату и адрес доставки, чтобы получить товар в удобное для вас время.</p>
				</div>
			
				<div role="tab-body" id="2" class="">
					<p>Вы сможете оплатить свой заказ наличными курьеру при получении или банковской картой при оформлении заказа на сайте.</p>
<p>Мы принимаем карты Visa и MasterCard.</p>
<p>Мы не обрабатываем вашу банковскую информацию и не храним данные ваших карт. Все транзакции проходят через защищенные сервера банка «Открытие», предоставляющего нам услуги интернет эквайринга.</p>
				</div>
			
				<div role="tab-body" id="4" class="">
					<p>Если у вас есть промо-код, дающий право скидки, введите его в поле Промо-код в своей Корзине с покупками. Промо-код будет автоматически посчитан системой и вы увидите размер своей скидки и общую сумму покупки с учетом скидки.</p>
				</div>
			
				<div role="tab-body" id="3" class="">
					<p>100% уверенность в le génépi! <br>
Если наше средство не подойдет, мы вернем вам деньги.  </p>
<p>Вы купили средство от le génépi и оно не понравилось вашей коже?<br>
Можете вернуть его в любой момент* и получить всю сумму обратно! <br>
<small>*не менее половины средства</small></p>

				</div>
			
				<div role="tab-body" id="5" class="">
					<p>Получите Бесплатную консультацию от ведущего косметолога le génépi в России, воспользовавшись нашим сервисом он-лайн косметолог. </p>
<p>На ваши вопросы, отвечает практикующий косметолог с многолетним опытом работы с косметикой le génépi.</p>
<p>Опишите свою кожу и потребность в уходе максимально подробно, чтобы получить наиболее точную консультацию. Она будет содержать советы по средствам le génépi, а также по процедуре ухода.</p>
				</div>
			
				<div role="tab-body" id="6" class="">
					Все растительные компоненты марки имеют Европейские экологические сертификаты (Ecocert) и собираются 
с соблюдением правил Ответственного производства EFT (Ecocert fair trade in the spirit of solidarity and responsibility).
				</div>
						
		</div>
	</div>
</div>



		</main>


<?php get_footer(); ?>