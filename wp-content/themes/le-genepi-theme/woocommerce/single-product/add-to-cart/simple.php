<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product;

if ( ! $product->is_purchasable() ) {
	return;
}
?>

<?php if ( $product->is_in_stock() ) : ?>
	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>



<form class="cart" method="post" enctype='multipart/form-data'>
	 	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>




	 	<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>




	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
<?php endif;?>