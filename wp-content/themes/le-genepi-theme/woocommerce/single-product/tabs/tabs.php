<?php


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );


if ( ! empty( $tabs ) ) : ?>

			<ul id="producttabs" class="nav nav-tabs nav-justified title-deep-blue" role="tablist">
	        <?php $i = 0;?>
			<?php foreach ( $tabs as $key => $tab ) : ?>
			<?php if($i > 0) :?>	
			<li role="presentation" <?php if($i == 1) :?> class="active" <?php endif;?>>
				<a href="#<?php echo $key;?>" aria-controls="<?php echo $key;?>" role="tab" data-toggle="tab">
				<span>
					<?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?>
				</span>	
				</a>
			</li>
			 <?php endif;?>
				<?php $i++;?>
			<?php endforeach; ?>
			</ul>
		
		<div class="tab-content">
			 <?php $j = 0;?>
		<?php foreach ( $tabs as $key => $tab ) : ?>
			<?php if($j > 0) :?> 
			<?php call_user_func( $tab['callback'], $key, $tab ); ?>
			<?php endif;?>
			<?php $j++;?>
		<?php endforeach; ?>
		</div>

<?php endif; ?>
