<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post, $product;

?>

<?php						
if(has_post_thumbnail()) {
	
	$props            = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
	$image            = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
				
				'alt'    => $props['alt'],
				'style' => 'display:none;',
				'itemprop' => 'image'
			) );

	echo apply_filters('woocommerce_single_product_image_html',
			
			sprintf(
			'<div class="ref-img-back ref-img-back-400" style="background-image:url(%s);">
				%s
			</div>
				<div class="ref-social">
						<div class="formal">
							<ul>
								<li><p itemprop="productID"> %s </p></li>
								<li><p > %s</p></li>
							</ul>
						</div>

						<div class="product-actions">
							
						  <span class=""><b>%s %s</b></span>

						  <button id="purchaseNow" type="button" class="btn btn-default btn-sm btn-title-deep-blue">в корзину</button>

						  </div>
				</div>',
			esc_url($props['url']),
				$image,
				$product->get_sku(),
				get_post_meta ( $post->ID , "_volume"  , true ),
				$product->get_price(),
				get_woocommerce_currency_symbol(),
				$product->get_sku()
			),
			$post->ID
	
	);		
			
		
	
}

?>