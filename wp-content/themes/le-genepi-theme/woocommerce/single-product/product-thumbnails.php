<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post, $product, $woocommerce;

$attachment_ids = $product->get_gallery_image_ids();

if ( $attachment_ids && has_post_thumbnail() ) {
	foreach ( $attachment_ids as $attachment_id ) {
		echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf(
			'<div class="col-xs-4">
									<div class="utp-icon" style="background-image:url(%s);"></div>
									<p class="text-center">%s</p>
								</div>',
			wp_get_attachment_image_src( $attachment_id, 'shop_full' )[0], wp_get_attachment_caption($attachment_id)
		));
	}
}

?>
