<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<a  href="<?php echo esc_url( wc_get_checkout_url() );?>" class="checkout-button button alt wc-forward btn btn-primary btn-blue">
	<?php esc_html_e( 'Proceed to checkout', 'woocommerce' ); ?>
</a>
