<?php


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}



wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>

<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
	<?php do_action( 'woocommerce_before_cart_table' ); ?>


	<div class="panel cart shop_table shop_table_responsive cart woocommerce-cart-form__contents">
		<h2>Корзина</h2>
		<?php do_action( 'woocommerce_before_cart_contents' ); ?>	
		<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
							<div id="cart-head"  class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>"></div>
							<div id="cart-body">
						<div class="panel cart-item">
							<?php
								$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(array(180,222)), $cart_item, $cart_item_key );

								if ( ! $product_permalink ) {
									echo $thumbnail;
								} else {
									printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail );
								}
							?>



							<div class="cart-item-description">
								<h3 class="name"><?php
								if ( ! $product_permalink ) {
									echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;';
								} else {
									echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key );
								}

								// Meta data.
								echo wc_get_formatted_cart_item_data( $cart_item );

								// Backorder notification.
								if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
									echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
								}
								?></h3>
								<?php print_r($product); ?>

								<p class="alt-name"><?php echo $_product->short_description; ?></p>
								<p class="ref"><?php echo $_product->sku; ?></p>
								<p class="vol"><?php echo get_post_meta ( $product->ID , "_volume"  , true ); ?></p>
							</div>
							<div class="cart-item-actions">
								<div class="cart-item-price product-price"  style="margin: 0.5em">
									<?php
										echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
									?>
								</div>
								<div class="cart-item-quantity product-quantity" style="margin: 0.5em">
									<?php
									if ( $_product->is_sold_individually() ) {
										$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
									} else {
										$product_quantity = woocommerce_quantity_input( array(
											'input_name'    => "cart[{$cart_item_key}][qty]",
											'input_value'   => $cart_item['quantity'],
											'max_value'     => $_product->get_max_purchase_quantity(),
											'min_value'     => '0',
											'product_name'  => $_product->get_name(),
										), $_product, false );
									}

									echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
									?>
								</div>
								<div class="cart-item-sum product-subtotal"  style="margin: 0.5em">
								<?php
										echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
								?>
								
								</div>
								<div class="cart-item-del"  style="margin: 0.5em"><?php
								// @codingStandardsIgnoreLine
								echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
									'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">X</a>',
									esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
									__( 'Remove this item', 'woocommerce' ),
									esc_attr( $product_id ),
									esc_attr( $_product->get_sku() )
								), $cart_item_key );
							?></div>
							</div>
						</div>
					</div>	

					<?php
			}
		}
	 ?>
	<script>
        jQuery('div.woocommerce').on('change', '.qty', function(){
            jQuery("[name='update_cart']").removeAttr("disabled").trigger("click");
        });
    </script>		

		<div id="cart-foot">


			<?php do_action( 'woocommerce_cart_contents' ); ?>	

			<div id="cart-code">
				<div style="display: flex; ">

					<?php if ( wc_coupons_enabled() ) { ?>
						
							<input type="text" name="coupon_code" style="  flex-grow: 1; border-radius: .4rem 0 0 .4rem; border-right: none;" class="input-text" id="coupon_code" value="" 
							placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> 
							
							<button id="check_promo" type="submit"  name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>" style="width: 4.5rem; background-color:#008000; color:white; border-color:#008000; border-radius: 0 .4rem .4rem 0;"><i class="fa fa-check"></i></button>
							
							
							<?php do_action( 'woocommerce_cart_coupon' ); ?>

						
					<?php } ?>
					
					<?php do_action( 'woocommerce_cart_actions' ); ?>

					<?php wp_nonce_field( 'woocommerce-cart' ); ?>
					
						
						
				
				</div>

			</div>

			<?php do_action( 'woocommerce_cart_collaterals' ); ?>
			
			<div id="cart-actions" class="wc-proceed-to-checkout">
				<a href="/product-category/products/" role="shopping" class="btn btn-default">продолжить покупки</a>
				<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
					
			</div>
		</div>
		<button hidden="true" type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>

	</div>



	
			<?php do_action( 'woocommerce_after_cart_contents' ); ?>
		</form>
	<?php do_action( 'woocommerce_after_cart_table' ); ?>
</form>



<?php do_action( 'woocommerce_after_cart' ); ?>