<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

	<div class="panel-body panel-body-no-padding title-deep-blue">
		<div class="row">
			<?php dynamic_sidebar('product_top_links'); ?>
			<div class="col-xs-8 col-xs-offset-4">
				<ul id="topnav" class="nav nav-pills nav-justified title-deep-blue">
				  <li role="presentation"><a href="/ru/shipping/#1" target="_blank"><span>Бесплатная доставка</span></a></li>
				  <li role="presentation"><a href="/ru/shipping/#3" target="_blank"><span>100% уверенности в le génépi</span></a></li>
				  <li role="presentation"><a href="#testdrive"><span>Тест-драйв</span></a></li>
				  <li role="presentation"><a href="/ru/shipping/#6" target="_blank"><span>ECOCERT, FairTraid</span></a></li>
				</ul>
				<hr class="title-deep-blue">
			</div>
		</div>
		<div id="description" class="row row_ref_descript">
			<div class="col-xs-4 img_centred ref-img">
				<?php do_action( 'woocommerce_before_single_product' ); ?>
					<?php do_action('woocommerce_before_single_product_summary');?>


										
					
							
			</div>
			<div class='col-xs-8 ref-des'>
						
						<?php do_action('woocommerce_single_product_summary'); ?>
						
						
						<span itemprop="description" style="display:none">
							 
							
						</span> 

						
						
						<div class="row utp-area">
							<?php do_action('woocommerce_product_thumbnails'); ?>
						</div>
							<div class="soc-icons">
								<ul class="list-inline">
									<li>
										Поделиться:
									</li>
									<li>
										<a class="btn btn-social-icon btn-facebook btn-sm btn-social-color"
											href=<?php echo "https://facebook.com/sharer.php?u=" . get_permalink(); ?>
											target="_blank" onfocus="if(this.blur)this.blur()">
											<i class="fa fa-facebook"></i>
										</a>								
									</li>
									<li>
										<a class="btn btn-social-icon btn-vk btn-sm btn-social-color"
										   href=<?php echo "http://vk.com/share.php?url=" . get_permalink(); ?> 
										   target="_blank" onfocus="if(this.blur)this.blur()">
											<i class="fa fa-vk"></i>
										</a>
									</li>
									<li>
										<a class="btn btn-social-icon btn-pinterest btn-sm btn-social-color" 
											href=<?php echo "//ru.pinterest.com/pin/create/button/?url=" . get_permalink(); ?>
											target="_blank" onfocus="if(this.blur)this.blur()">
											<i class="fa fa-pinterest"></i>
										</a>
	<!--
										<script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script>
	-->
									</li>
								</ul>
						</div>
			</div>	

		</div> <!-- row description-->	
		
		<hr class="title-deep-blue">
		<div class="row">
			<div class="col-xs-12">
				<div class="shipping-terms product-tabs">
				<?php do_action('woocommerce_after_single_product_summary'); ?>	

				</div>
			</div>
		</div>
				
	</div>