<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;


if($price_html = $product->get_price() ){
	echo apply_filters ('woocommerce_loop_add_to_cart_link',
	sprintf('<div class="shop-product-actions">
			<span class=""><b>%s %s</b></span>
			<button type="button" class="btn btn-default" data-quantity="%s" data-product_id="%s" data-product_sku="%s">в корзину</button>
			<a href="%s" type="button" class="btn btn-default ">подробнее</a>
	</div>', 
	$price_html,
	esc_attr(get_woocommerce_currency_symbol()),
	esc_attr(isset($quantity) ? $quantity : 1),
	esc_attr($product->id),
	esc_attr($product->get_sku),
	get_the_permalink()
	));
}
?>

