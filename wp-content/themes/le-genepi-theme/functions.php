<?php 

global $opts;
if(function_exists('get_fields')){
  $opts = get_fields('option');
}else{
  $opts = [];
}

add_action( 'after_setup_theme', 'woocommerce_support' );

/* Changed excerpt length to 150 words*/
function my_excerpt_length($length) {
return 30;
}
add_filter('excerpt_length', 'my_excerpt_length');


function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
add_action( 'wp_enqueue_scripts', 'le_genepi_styles' );
add_action( 'wp_enqueue_scripts', 'le_genepi_scripts');

function le_genepi_styles() {
	wp_enqueue_style('slick', get_template_directory_uri() . '/assets/slick/slick.css');
	wp_enqueue_style('slick-theme', get_template_directory_uri() . '/assets/slick/slick-theme.css');
	
	// wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.css');
	wp_enqueue_style('PTSans-stylesheet', get_template_directory_uri() . '/assets/fonts/PTSans/stylesheet.css');
	wp_enqueue_style('bootstrap-datetimepicker', get_template_directory_uri() . '/assets/datapicker/css/bootstrap-datetimepicker.min.css');
	
	wp_enqueue_style('adaptive', get_template_directory_uri() . '/assets/css/adaptive.css');
	wp_enqueue_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css');
	//wp_enqueue_style('le-genepi', get_template_directory_uri() . '/assets/css/le-genepi.css');
	//wp_enqueue_style('bootstrap_min', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css');
}


function le_genepi_scripts() {
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', get_template_directory_uri() . '/assets/js/jquery-2.0.3.js');
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script('mask', get_template_directory_uri() . '/assets/js/jquery.mask.min.js', array('jquery'), null, true);
	wp_enqueue_script('inputmask', get_template_directory_uri() . '/assets/js/jquery.inputmask.js', array('jquery'), null, true);
	wp_enqueue_script('cookie', get_template_directory_uri() . '/assets/js/jquery.cookie.js', array('jquery'), null, true);
	wp_enqueue_script('docs', get_template_directory_uri() . '/assets/js/docs.js', array('jquery'), null, true);
	wp_enqueue_script('slick', get_template_directory_uri() . '/assets/slick/slick.min.js', array('jquery'), null, true);
	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.min.js', array('jquery'), null, true);
}   

add_filter('woocommerce_enqueue_styles', '__return_empty_array');

function my_options() {
	
	add_settings_field('phone','Телефон','display_phone','discussion');
	register_setting('discussion','my_phone');
	
}

function display_phone() {
	echo '<input type="text" class="regular-text" name="phone" value="'.esc_attr(get_option('phone')).'">';
}

add_action('admin_menu','my_options');

add_image_size( 'shop_img_size', 268, 331 );

register_nav_menus(array(
	'top' => 'Верхнее меню',
	'bottom' => 'Нижнее меню', 
	'store' => 'Меню магазина',
	'single_products' => 'Меню продуктов на странице товара'
));

class Custom_Walker_Single_Products_Menu extends Walker_Nav_Menu{
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){
		$html = '';
		global $post;
		if($depth == 0) {
			$categories = get_the_terms( $post->ID, 'product_cat' );
			$html .= '<li';
			foreach ($categories as $category) {
    			if(strripos($item->url, $category->slug)) {
    				$html .= ' class="section-menu-active" ';
    				break;
    			}
			}
			
            $html .= '><a href="%s" onfocus="if(this.blur)this.blur()">
							<p>
							%s';
            $output .= sprintf($html, $item->url,$item->title);
		}

		
	}

	public function end_el( &$output, $item, $depth = 0, $args = array()){
		if($depth == 0) {
			$output .= '</p>
						</a>
					</li>';
		}
	}
}

class Custom_Walker_Top_Menu extends Walker_Nav_Menu{

	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){
		$html = '';
		if($depth == 0) {
			$html .= '<li><a href="%s" onfocus="if(this.blur)this.blur()">  
			<span class="animated">
			<em>%s</em>
			<em class="hover">%s</em>
            </span></a>';
            $output .= sprintf($html, $item->url,$item->title,$item->title);
		}

		
	}

	public function end_el( &$output, $item, $depth = 0, $args = array()){
		if($depth == 0) {
			$output .= '</li>';
		}
	}
}

class Custom_Walker_Battom_Menu extends Walker_Nav_Menu{
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){
		$html = '';
		if($depth == 0) {
			$html .= '<div class="footer-item">
      			<p><a href="%s">%s';
            $output .= sprintf($html, $item->url,$item->title);
		}
		if($depth == 1) {
			$html .= '<li><a href="%s">%s';
			$output .= sprintf($html, $item->url,$item->title);
		}
	}

	public function end_el( &$output, $item, $depth = 0, $args = array()){
		if($depth == 0) {
			$output .= "</a></p></div>";
		}
		if($depth == 1){
			$output .= "</a></li>";
		}
	}
}


function woocommerce_template_loop_product_thumbnail() {
	
	echo '<h4><a href="'.get_the_permalink().'" target="_blank">'.get_the_title().'</a></h4>'.get_the_excerpt();
	echo '<img src="'.get_the_post_thumbnail_url().'">';
	if (get_post_meta(get_the_ID(), '_stock_status', true) == 'outofstock') {
		echo '<div>Нет в наличии</div>';
	} else {
 		echo '<p class="storage-green"><i class="fa fa-check"></i> в наличии</p>';
	}
}

function legenepi_widgets_init(){
	
	register_sidebar(array(
		'name' => 'products menu',
		'id' => 'products_menu',
		'description' => 'Меню продуктов',
		'before_widget' => '<div class="filter-item">',
		'after_widget' => '</div>',
		'before_title' => '<label for="skin-toogle">',
		'after_title' => '</label>'
	));

	register_sidebar(array(
		'name' => 'tabs menu',
		'id' => 'tabs_menu',
		'description' => 'Таб меню на странице продукта',
	));

	register_sidebar(array(
		'name' => 'product top links',
		'id' => 'product_top_links',
		'description' => 'Полезные ссылки в топе описания продукта' )
	);
}

add_action('widgets_init', 'legenepi_widgets_init');

add_filter('widget_nav_menu_args','change_menu','',4);

function change_menu($nav_menu_args, $nav_menu, $args, $instance) {

	 $nav_menu_args['container'] = 'false';
	 $nav_menu_args['items_wrap'] = '<select onchange="_change_category()" id="category" class="form-control">%3$s</select>';
	// $nav_menu_args['menu_class'] = "f_nav";
	// $nav_menu_args['link_before'] = "<li>";
	$nav_menu_args['walker'] = new Custom_Walker_Products_Menu;
	
	return $nav_menu_args;
}

class Custom_Walker_Products_Menu extends Walker_Nav_Menu{
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){
		$html = '';
		if($depth == 0) {
			$object = get_queried_object();
			$html .= '<option ';
			if($object->slug == $item->post_name) $html .= 'selected ';
			$html .= 'value="%s">%s';
            $output .= sprintf($html, $item->url,$item->title);

		}
	}
	public function end_el( &$output, $item, $depth = 0, $args = array()){
		if($depth == 0) {
			$output .= '</option>';
		}
	}
}
 
add_action( 'woocommerce_product_options_general_product_data', 'art_woo_add_volume' );
function art_woo_add_volume() {
	global $product, $post;
	echo '<div class="options_group">';
		woocommerce_wp_text_input( array(
		   'id'            => '_volume', 
		   'label'         => 'Объем', 
		   'placeholder'   => 'Введите объем',
		) );
	echo '</div>';
}

add_action( 'woocommerce_process_product_meta', 'art_woo_volume_save', 1 );
function art_woo_volume_save( $post_id ) {
   $woocommerce_text_field = $_POST['_volume'];
   if ( $woocommerce_text_field ) {
      update_post_meta( $post_id, '_volume', esc_attr( $woocommerce_text_field ) );
   }
}

add_action('woocommerce_before_single_product_summary', 'product_single_page');

function product_single_page(){
	if(is_product()){
		//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
	}
}

add_action('woocommerce_after_single_product_summary', 'after_product_single_page');

function after_product_single_page(){
	if(is_product()){
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
	}
}

add_filter( 'woocommerce_product_tabs', 'sb_woo_remove_reviews_tab', 98);
function sb_woo_remove_reviews_tab($tabs) {
 
unset($tabs['reviews']);
 
return $tabs;
}

add_action( 'woocommerce_product_options_general_product_data', 'art_woo_add_custom_tabs_fields' );
function art_woo_add_custom_tabs_fields() {
	global $product, $post;
  	echo '<div class="options_group"><p> Дополнительные вкладки на странице продукта </p>';

	woocommerce_wp_textarea_input( array(
	   'id'            => '_techtextarea', 
	   'label'         => 'Технологии',
	   'placeholder'   => 'Введите данные для отображения на странице продукта технолгоий', 
	   'rows'          => '10',
	   'col'           => '100',
	) );
	woocommerce_wp_textarea_input( array(
	   'id'            => '_ingredientstextarea', 
	   'label'         => 'Ингридиенты',
	   'placeholder'   => 'Введите данные для отображения на странице продукта ингридиентов', 
	   'rows'          => '10',
	   'col'           => '100',
	) );
	woocommerce_wp_textarea_input( array(
	   'id'            => '_usetextarea', 
	   'label'         => 'Использование',
	   'placeholder'   => 'Введите данные для отображения на странице продукта вариантов использования', 
	   'rows'          => '10',
	   'col'           => '100',
	) );
	woocommerce_wp_textarea_input( array(
	   'id'            => '_faqtextarea', 
	   'label'         => 'Вопросы',
	   'placeholder'   => 'Введите данные для отображения на странице продукта вопросов', 
	   'rows'          => '10',
	   'col'           => '100',
	) );
	echo '</div>';
}

add_action( 'woocommerce_process_product_meta', 'art_woo_custom_tabs_fields_save', 4 );
function art_woo_custom_tabs_fields_save( $post_id ) {
$woocommerce_tech_textarea = $_POST['_techtextarea'];
   if ( ! empty( $woocommerce_tech_textarea ) ) {
      update_post_meta( $post_id, '_techtextarea', esc_html( $woocommerce_tech_textarea ) );
   }
$woocommerce_ingredients_textarea = $_POST['_ingredientstextarea'];
   if ( ! empty( $woocommerce_ingredients_textarea ) ) {
      update_post_meta( $post_id, '_ingredientstextarea', esc_html( $woocommerce_ingredients_textarea ) );
   }
$woocommerce_use_textarea = $_POST['_usetextarea'];
   if ( ! empty( $woocommerce_use_textarea ) ) {
      update_post_meta( $post_id, '_usetextarea', esc_html( $woocommerce_use_textarea ) );
   }
$woocommerce_faq_textarea = $_POST['_faqtextarea'];
   if ( ! empty( $woocommerce_faq_textarea ) ) {
      update_post_meta( $post_id, '_faqtextarea', esc_html( $woocommerce_faq_textarea ) );
   }
}

add_filter( 'woocommerce_product_tabs', 'wpb_new_product_tabs' );
function wpb_new_product_tabs( $tabs ) {
    // Add the new tab
    $tabs['descript'] = array(
        'title'       => __( 'Описание', 'text-domain' ),
        'priority'    => 20,
        'callback'    => 'wpb_description_tab_content'
    );
    $tabs['technology'] = array(
        'title'       => __( 'Технологии', 'text-domain' ),
        'priority'    => 50,
        'callback'    => 'wpb_technology_tab_content'
    );
    $tabs['ingrid'] = array(
        'title'       => __( 'Ингридиенты', 'text-domain' ),
        'priority'    => 50,
        'callback'    => 'wpb_ingrid_tab_content'
    );
    $tabs['instruction'] = array(
        'title'       => __( 'Инструкция', 'text-domain' ),
        'priority'    => 50,
        'callback'    => 'wpb_instruction_tab_content'
    );
    $tabs['faqs'] = array(
        'title'       => __( 'Вопросы', 'text-domain' ),
        'priority'    => 50,
        'callback'    => 'wpb_faq_tab_content'
    );
    return $tabs;
}

function wpb_description_tab_content() {
    // The new tab content
	    echo '<div role="tabpanel" class="tab-pane active" id="descript">
								<div id="" class="row row_ref_tab">';
	    echo the_content();         
	    echo '</div></div>'; 
}

function wpb_technology_tab_content() {
    	global $post;
	    echo '<div role="tabpanel" class="tab-pane" id="technology">
								<div id="technology" class="row row_ref_tab">';

				
		echo htmlspecialchars_decode(get_post_meta($post->ID, "_techtextarea", true ), ENT_QUOTES);					

	   	          
	    echo '</div></div>'; 
}

function wpb_ingrid_tab_content() {
     	global $post;
	    echo '<div role="tabpanel" class="tab-pane" id="ingrid">
								<div id="technology" class="row row_ref_tab">';
	    echo htmlspecialchars_decode(get_post_meta($post->ID, "_ingredientstextarea", true ), ENT_QUOTES);         
	    echo '</div></div>'; 
}

function wpb_instruction_tab_content() {
   		global $post;
	    echo '<div role="tabpanel" class="tab-pane" id="instruction">
								<div id="technology" class="row row_ref_tab">';
	    echo htmlspecialchars_decode(get_post_meta($post->ID, "_usetextarea", true ), ENT_QUOTES);            
	    echo '</div></div>'; 
}

function wpb_faq_tab_content() {
    	global $post;
	    echo '<div role="tabpanel" class="tab-pane" id="faqs">
								<div id="technology" class="row row_ref_tab">';
	    echo htmlspecialchars_decode(get_post_meta($post->ID, "_faqtextarea", true ), ENT_QUOTES);          
	    echo '</div></div>'; 
}


function print_sections(){
  global $opts;
  if( function_exists('get_fields') ):
    if( have_rows('content') ):
      $counter_i = 0;
      while ( have_rows('content') ) : the_row();
        $row = get_row(true);
        if($row['hide_section'] && !isset($_GET['debug'])) continue;
        $layout = get_row_layout();
        if(true){
          $id = 'section_' . $counter_i++;
        }
        switch ($layout) {
        	case 'about_text':?>
	    		<div class="about-item">
					<h2><?=$row['title'];?></h2>
					<p><?=$row['description'];?></p>
				</div>
		       	<?php
		       	break;
   	       	case 'advices_img_txt':?>
   				<div class='row consult-content'>        	
					<div class="col-xs-6 img-consult-base">
						<a href="/ru/products/cleansers/ref.AT.1998/">
							<div class='img_consult' style='background-image:url(<?php echo $row['advice_img']['sizes']['large'];?>);'>
							</div>
						</a>
					</div>
					<div class="col-xs-6 des-consult-base">
						<div class="consult-description">
							<h3><?=$row['title'];?></h3>
							<p><?=$row['description'];?></p>
							<?php if (strlen($row['advice_cosmetologist'])>0):?>
							<b class="consult-in">Совет косметолога:</b> <hr class="inline">
							<p><?=$row['advice_cosmetologist'];?>
							<?php endif;?>
							</p>
						</div>
					</div>
				</div>
				<hr>
				<?php
		       	break;
		    case 'advices_txt_img':?>
    			<div class='row consult-content'>
			    	<div class="col-xs-6 des-consult-base">
				    	<div class="consult-description">
				    		<h3><?=$row['title'];?></h3>
							<p><?=$row['description'];?></p>
							<?php if (strlen($row['advice_cosmetologist'])>0):?>
							<b class="consult-in">Совет косметолога:</b> <hr class="inline">
							<p><?=$row['advice_cosmetologist'];?></p>
						<?php endif;?>
						</div>
					</div>
					<div class="col-xs-6 img-consult-base">
						<a href="/ru/products/daily_care/ref.AC.025/">
							<?php if (isset($row['advice_img']) && isset($row['advice_img']['sizes'])):?>
							<div class='img_consult' style='background-image:url(<?php echo $row['advice_img']['sizes']['large'];?>);'>
							</div>
						<?php endif;?>
						</a>
					</div>
				</div>
				<hr>
			    <?php 
			    break;
		}
      endwhile;
    endif;
  endif;
}
?>
