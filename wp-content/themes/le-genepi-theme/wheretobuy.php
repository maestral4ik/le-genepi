<?php // Template Name: Где купить?>
<?php get_header();?>
<main>
	
		<div class='container container-white'>			
				
<div class="row">
	<div class="panel panel-default link-to-shop">
		<div class="panel-body">
			<div class='row'>
					<a href="/ru/shop/">
					<div id="online-shop"></div>
					</a>
			</div>
			<hr>
			
<div class="row">
	<div class="col-xs-12">
	<!--
		<p class='buy-note'>	
			<b>Бесплатная доставка</b> всем клиентам марки <b>le génépi</b>.				
		</p>
	-->
		<p class='buy-note'>	
			<b>Оплата</b> производится курьеру <a href="http://dpd.ru/ru/private/index.html">DPD</a> <b>при получении заказа</b>.				
		Подробнее об условиях <a href="/ru/shipping/">доставки и оплаты</a>.</p> 
		<p class='buy-note'>Есть вопросы? Позвоните нам по тел. +7(812)929-38-83 или закажите Обратный звонок.
		</p>
	</div>
</div>

		</div>
	</div>
</div>

<div class="row">
	<div class="panel panel-default">
		<div class="panel-body">
			<div class="row">
				<div class="col-xs-12 retail-title-parent">
					<h3 class="retail-title">Приобретайте посметику в магазинах и салонах города</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4 drop-city-parent">
					<div class="dropdown drop-city">
					  <button class="btn btn-default dropdown-toggle" type="button" id="dropdown-city" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						Санкт-Петербург
						<span class="caret"></span>
					  </button>
					  <ul class="dropdown-menu" aria-labelledby="dropdown-city">
						
								<li>
									<a href="/ru/retail/spb/">
										Санкт-Петербург
									</a>
								</li>
						
								<li>
									<a href="/ru/retail/msk/">
										Москва
									</a>
								</li>
						
					  </ul>
					</div>
				</div>
			</div>
			<div class="row">	
				<div class="col-xs-7 retail-shops-list">
					<div class="row">
						
							<div class="col-xs-6" itemscope itemtype="http://schema.org/Organization">
								<div class="row retail-shop" >
									<div class="col-xs-12" >
										<h3 itemprop="name">Студия красоты «Счастье» <i class="fa fa-map-marker" style="color:#000000;"></i></h3>
										
										
										
									</div>
								</div>
								
									<div class="row retail-addr" itemprop='subOrganization' itemscope itemtype="http://schema.org/Organization">
										<div class="col-xs-12">
											
											<p itemprop="address">Комендантский пр. 51 к. 1</p>
											
											
											<p itemprop="telephone">Телефон: +7 (921) 574-2797</p>
											
										</div>
									</div>
																				
							</div>					
						
							<div class="col-xs-6" itemscope itemtype="http://schema.org/Organization">
								<div class="row retail-shop" >
									<div class="col-xs-12" >
										<h3 itemprop="name">SHOWROOM le génépi <i class="fa fa-map-marker" style="color:#a3d0f0;"></i></h3>
										
										
										
									</div>
								</div>
								
									<div class="row retail-addr" itemprop='subOrganization' itemscope itemtype="http://schema.org/Organization">
										<div class="col-xs-12">
											
											<p itemprop="address">Ждем к нам в гости по предварительной записи:</p>
											<p itemprop="address">Большой пр. ПС, д. 74</p>
											
											<p itemprop="telephone">Телефон: +7 (812) 929 3883</p>
											<p itemprop="email">E-mail: partner@le-genepi.ru</p>
										</div>
									</div>
																				
							</div>					
						
							<div class="col-xs-6" itemscope itemtype="http://schema.org/Organization">
								<div class="row retail-shop" >
									<div class="col-xs-12" >
										<h3 itemprop="name">BeloNika Shop <i class="fa fa-map-marker" style="color:#793d0e;"></i></h3>
										<p><a href="http://www.shop.belonika.ru" target="_blank" itemprop='url'>www.shop.belonika.ru</a></p>
										
										
									</div>
								</div>
																				
							</div>					
						
							<div class="col-xs-6" itemscope itemtype="http://schema.org/Organization">
								<div class="row retail-shop" >
									<div class="col-xs-12" >
										<h3 itemprop="name">Частный клуб <i class="fa fa-map-marker" style="color:#56db40;"></i></h3>
										
										
										
									</div>
								</div>
								
									<div class="row retail-addr" itemprop='subOrganization' itemscope itemtype="http://schema.org/Organization">
										<div class="col-xs-12">
											
											<p itemprop="address">Соляной пер. 4</p>
											
											<p><a href="http://www.salonprivateclub.ru" target="_blank" itemprop='url'>www.salonprivateclub.ru</a></p>
											<p itemprop="telephone">Телефон: +7 (812) 900 00 35</p>
											
										</div>
									</div>
																				
							</div>					
						
							<div class="col-xs-6" itemscope itemtype="http://schema.org/Organization">
								<div class="row retail-shop" >
									<div class="col-xs-12" >
										<h3 itemprop="name">Гомеосинергетический центр «Интеграта» <i class="fa fa-map-marker" style="color:;"></i></h3>
										
										
										
									</div>
								</div>
								
									<div class="row retail-addr" itemprop='subOrganization' itemscope itemtype="http://schema.org/Organization">
										<div class="col-xs-12">
											
											<p itemprop="address">ул. Рентгена, 7, оф. 361</p>
											
											
											<p itemprop="telephone">Телефон: +7 (812) 347-67-74</p>
											
										</div>
									</div>
																				
							</div>					
						
							<div class="col-xs-6" itemscope itemtype="http://schema.org/Organization">
								<div class="row retail-shop" >
									<div class="col-xs-12" >
										<h3 itemprop="name">Чайковского 26 <i class="fa fa-map-marker" style="color:#ffa921;"></i></h3>
										
										
										
									</div>
								</div>
								
									<div class="row retail-addr" itemprop='subOrganization' itemscope itemtype="http://schema.org/Organization">
										<div class="col-xs-12">
											
											<p itemprop="address">ул. Чайковского, д. 26</p>
											
											<p><a href="http://www.salon26.ru" target="_blank" itemprop='url'>www.salon26.ru</a></p>
											<p itemprop="telephone">Телефон: +7 (921) 973-1453</p>
											<p itemprop="email">E-mail: salon26@list.ru</p>
										</div>
									</div>
																				
							</div>					
						
							<div class="col-xs-6" itemscope itemtype="http://schema.org/Organization">
								<div class="row retail-shop" >
									<div class="col-xs-12" >
										<h3 itemprop="name">Имидж студия «Корона» <i class="fa fa-map-marker" style="color:#42B242;"></i></h3>
										
										
										
									</div>
								</div>
								
									<div class="row retail-addr" itemprop='subOrganization' itemscope itemtype="http://schema.org/Organization">
										<div class="col-xs-12">
											
											
											<p itemprop="address">пр. Испытателей, д. 33</p>
											<p><a href="http://koronaimage.ru" target="_blank" itemprop='url'>koronaimage.ru</a></p>
											<p itemprop="telephone">Телефон: +7 (911) 276 0906</p>
											
										</div>
									</div>
																				
							</div>					
						
							<div class="col-xs-6" itemscope itemtype="http://schema.org/Organization">
								<div class="row retail-shop" >
									<div class="col-xs-12" >
										<h3 itemprop="name">Royal Beauty <i class="fa fa-map-marker" style="color:#ed5050;"></i></h3>
										
										
										
									</div>
								</div>
								
									<div class="row retail-addr" itemprop='subOrganization' itemscope itemtype="http://schema.org/Organization">
										<div class="col-xs-12">
											
											<p itemprop="address">Полтавский проезд 2-В</p>
											
											<p><a href="http://royalbeautyspb.ru" target="_blank" itemprop='url'>royalbeautyspb.ru</a></p>
											<p itemprop="telephone">Телефон: +7 (905) 216 1818</p>
											<p itemprop="email">E-mail: info@royalbeautyspb.ru</p>
										</div>
									</div>
																				
							</div>					
						
							<div class="col-xs-6" itemscope itemtype="http://schema.org/Organization">
								<div class="row retail-shop" >
									<div class="col-xs-12" >
										<h3 itemprop="name">Салон красоты «Николь» <i class="fa fa-map-marker" style="color:#000000;"></i></h3>
										
										
										
									</div>
								</div>
								
									<div class="row retail-addr" itemprop='subOrganization' itemscope itemtype="http://schema.org/Organization">
										<div class="col-xs-12">
											
											<p itemprop="address">г. Сестрорецк, Приморское шоссе, д. 273</p>
											
											<p><a href="http://www.snicole.ru" target="_blank" itemprop='url'>www.snicole.ru</a></p>
											<p itemprop="telephone">Телефон: +7 (812) 437-80-80</p>
											<p itemprop="email">E-mail: ssnicole@mail.ru</p>
										</div>
									</div>
																				
							</div>					
						
							<div class="col-xs-6" itemscope itemtype="http://schema.org/Organization">
								<div class="row retail-shop" >
									<div class="col-xs-12" >
										<h3 itemprop="name">Имидж студия Натальи Никитиной <i class="fa fa-map-marker" style="color:#ed77cd;"></i></h3>
										
										
										
									</div>
								</div>
								
									<div class="row retail-addr" itemprop='subOrganization' itemscope itemtype="http://schema.org/Organization">
										<div class="col-xs-12">
											
											
											<p itemprop="address">ул. Плуталова, д. 4</p>
											<p><a href="http://www.natanik.com" target="_blank" itemprop='url'>www.natanik.com</a></p>
											<p itemprop="telephone">Телефон: +7 (921) 304 5420</p>
											<p itemprop="email">E-mail: nikitina-make-up@yandex.ru</p>
										</div>
									</div>
																				
							</div>					
						
							<div class="col-xs-6" itemscope itemtype="http://schema.org/Organization">
								<div class="row retail-shop" >
									<div class="col-xs-12" >
										<h3 itemprop="name">SPA «Оникс» <i class="fa fa-map-marker" style="color:#ffd21e;"></i></h3>
										<p><a href="http:// " target="_blank" itemprop='url'> </a></p>
										
										
									</div>
								</div>
								
									<div class="row retail-addr" itemprop='subOrganization' itemscope itemtype="http://schema.org/Organization">
										<div class="col-xs-12">
											
											<p itemprop="address">Константиновский пр., д. 23</p>
											
											
											<p itemprop="telephone">Телефон: +7 (812) 583 8303</p>
											
										</div>
									</div>
																				
							</div>					
						
					</div>
				</div>
				<div class="col-xs-5 mapparent">
					<div class="row mapcontainer">
						<div class="col-xs-12" id="citymap">
						</div>	
						<script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=q0y7HGJUrZzS1d1LtMz2gmIElwgYrtc1&amp;height=400&amp;lang=ru_RU&amp;sourceType=constructor&id=citymap"></script>
					</div>					
				</div>
			</div>
		</div>
	</div>
</div>		


		
		</div>
</main>
<?php get_footer();?>