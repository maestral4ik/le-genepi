<?php // Template Name: О марке?>
<?php 
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' ); // возвращает массив параметров миниатюры

get_header(); ?>
<main>
	<div role="page" class="container-flex">
		<div class="panel panel-nopadding about-header">
			<img src="<?php echo $thumbnail_attributes[0];?>" alt="О Le genepi">
		</div>
		<div class="panel about">
		<?php print_sections(); ?>
		</div>
	</div>
</main>
<?php get_footer(); ?>
