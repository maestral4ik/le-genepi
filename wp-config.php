<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'legenepi');

/** Имя пользователя MySQL */
define('DB_USER', 'admin');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'admin');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{,1=`[L@FO/F+qI5 bv>mAuj4_YF-cc(*M~PmR,_%IoI(tA,RA(G}l+mn0/v8}c$');
define('SECURE_AUTH_KEY',  '_Rl7AC{nt/&KD&0u08CXEoY)&;h]<M4<x3_[?|9F0**dF?#dH0:7ryuQSg$Yd$au');
define('LOGGED_IN_KEY',    'Wm#|D!so5e?U97tjd]K82R:AwUAKNM3&j.(Y%4Vn~4#O]Ia=HOBblC}8}O8KUp~1');
define('NONCE_KEY',        '8eBGH91Vu=NF*|/p<2i6Vn?UTn%rl_iN#0E-A%)sXm:~+*YK3<7dVKd?n{?h~f6s');
define('AUTH_SALT',        'iA~Vpk}J^S8Q{mK|L4_+V(<2P,lw+I@n)>1FNbdmlHKr!DZ3x1R^Cvk,=?Q1**t$');
define('SECURE_AUTH_SALT', '`6.7h3A>k#^Qg%gJ.+[oO^6JsB>#V?mx4(e!S`nRy @m h`2vQe1N#%r_5P]hQx!');
define('LOGGED_IN_SALT',   '{OeM]Y>/@B~JNo_*{HYaPPC%r*mXsH@FCz}g|xn&0;GAe2r#]0khJa!hV@|}wmC1');
define('NONCE_SALT',       'pWG+=jaaS>7:XYykmHoM;&luDXk^r2HNPb1nk&Y.Mkkl4,,hrK{N^x+62Zx)u*F8');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define( 'RELOCATE', true );
/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
